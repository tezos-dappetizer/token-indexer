import { DbContext, IndexingCycleHandlerUsingDb } from '@tezos-dappetizer/database';
import { Block } from '@tezos-dappetizer/indexer';
import { injectLogger, Logger } from '@tezos-dappetizer/utils';
import { noop } from 'ts-essentials';
import { delay, inject, singleton } from 'tsyringe';

import { CompositeDbUpdater } from './db-updaters/composite-db-updater';
import { TokenMetadataBackgroundDispatcher } from './metadata/token/token-metadata-background-dispatcher';
import { TokenIndexingData } from './token-indexing-data';

@singleton()
export class TokenIndexingHandler implements Required<IndexingCycleHandlerUsingDb<TokenIndexingData>> {
    // Empty methods to fully implement the interface.
    readonly beforeIndexersExecute = noop;
    readonly rollBackOnReorganization = noop;

    constructor(
        private readonly dbUpdater: CompositeDbUpdater,
        @injectLogger(TokenIndexingHandler) private readonly logger: Logger,
        @inject(delay(() => TokenMetadataBackgroundDispatcher)) private readonly tokenMetadataDispatcher: TokenMetadataBackgroundDispatcher,
    ) {}

    createContextData(_block: Block, _dbContext: DbContext): TokenIndexingData {
        return {
            changes: [],
            keysOfTokensPendingMetadata: [],
        };
    }

    async afterIndexersExecuted(block: Block, dbContext: DbContext, contextData: TokenIndexingData): Promise<void> {
        this.logger.logInformation('Indexed {changes}.', {
            block: {
                level: block.level,
                hash: block.hash,
                timestamp: block.timestamp,
            },
            changes: contextData.changes.map(c => `${c.type} ${c.amount?.toFixed() ?? ''} of ${c.tokenId.toFixed()} ${c.contract.address}`),
            fullChanges: this.logger.isTraceEnabled ? contextData.changes : undefined,
        });

        await this.dbUpdater.applyChanges(dbContext.transaction, contextData, block);
    }

    afterBlockIndexed(_block: Block, contextData: TokenIndexingData): void {
        for (const key of contextData.keysOfTokensPendingMetadata) {
            this.tokenMetadataDispatcher.updateOnBackground(key);
        }
    }
}
