import {
    CONTRACT_PROVIDER_DI_TOKEN,
    ContractProvider,
    IndexingDappetizerConfig,
    SELECTIVE_INDEXING_CONFIG_DI_TOKEN,
    SelectiveIndexingConfig,
} from '@tezos-dappetizer/indexer';
import {
    BackgroundWorker,
    ConfigError,
    dumpToString,
    getConstructorName,
    injectLogger,
    isNullish,
    keyof,
    Logger,
} from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { TokenIndexerConfig } from './token-indexer-config';
import { TOKEN_INDEXER_MODULE_ID, TokenIndexerInternalConfig } from './token-indexer-internal-config';
import { CompositeContractTypeResolver } from '../indexers/composite-contract-type-resolver';

@singleton()
export class TokenIndexerConfigValidator implements BackgroundWorker {
    readonly name = getConstructorName(this);

    constructor(
        private readonly tokenIndexerConfig: TokenIndexerInternalConfig,
        @inject(SELECTIVE_INDEXING_CONFIG_DI_TOKEN) private readonly indexingConfig: SelectiveIndexingConfig,
        @inject(CONTRACT_PROVIDER_DI_TOKEN) private readonly contractProvider: ContractProvider,
        private readonly contractTypeResolver: CompositeContractTypeResolver,
        @injectLogger(TokenIndexerConfigValidator) private readonly logger: Logger,
    ) {}

    private get contractsConfigPropertyPath(): string {
        return `${this.indexingConfig.configPropertyPath}.${keyof<IndexingDappetizerConfig>('contracts')}`;
    }

    private get contractNamesConfigPropertyPath(): string {
        return `${this.tokenIndexerConfig.configPropertyPath}.${keyof<TokenIndexerConfig>('contractNames')}`;
    }

    async start(): Promise<void> {
        try {
            if (!this.tokenIndexerConfig.contractNames) {
                return;
            }
            if (!this.indexingConfig.contracts) {
                this.throwConfigError(`If selective indexing is disabled (nothing specified) in '${this.contractsConfigPropertyPath}',`
                    + ` then '${keyof<TokenIndexerConfig>('contractNames')}' cannot be specified.`);
            }

            await Promise.all(this.tokenIndexerConfig.contractNames.map(async contractName => {
                const contractAddresses = this.indexingConfig.contracts?.find(c => c.name === contractName)?.addresses;
                if (!contractAddresses) {
                    this.throwConfigError(`Name '${contractName}' is missing in '${this.contractsConfigPropertyPath}' containing`
                        + ` ${dumpToString(this.indexingConfig.contracts?.map(c => c.name).filter(n => !isNullish(n)))}.`);
                }

                const canBeIndexed = await this.canIndexContractAddresses(contractAddresses);
                if (!canBeIndexed) {
                    this.throwConfigError(`Name '${contractName}' resolves to addresses ${dumpToString(contractAddresses)}`
                        + ` from '${this.contractsConfigPropertyPath}' but corresponding contracts cannot be indexed (do not comply to FA1.2 nor FA2)`
                        + ` by ${TOKEN_INDEXER_MODULE_ID}.`);
                }
            }));
        } catch (error) {
            if (error instanceof ConfigError) {
                throw error;
            }
            this.logger.logError('Failed to validate {configPropertyPath} from {configFile} so they may not make sense in the runtime. {error}', {
                configPropertyPath: this.contractNamesConfigPropertyPath,
                configFile: this.tokenIndexerConfig.configFilePath,
                error,
            });
        }
    }

    private async canIndexContractAddresses(addresses: readonly string[]): Promise<boolean> {
        const contractTypes = await Promise.all(addresses.map(async address => {
            const contract = await this.contractProvider.getContract(address);
            return this.contractTypeResolver.resolveContractType(contract);
        }));
        return contractTypes.some(t => !isNullish(t));
    }

    private throwConfigError(reason: string): never {
        throw new ConfigError({
            filePath: this.tokenIndexerConfig.configFilePath,
            propertyPath: this.contractNamesConfigPropertyPath,
            value: this.tokenIndexerConfig.contractNames,
            reason,
        });
    }
}
