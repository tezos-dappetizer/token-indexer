import { NonEmptyArray } from 'ts-essentials';

export interface TokenIndexerConfig {
    logIncompatibleContracts?: boolean;
    logIncompatibleContractsThreshold?: number;
    skipGetMetadata?: boolean;
    metadataUpdateParallelism?: number;

    /**
     * The selective indexing of particular smart contract names referencing globally configured selective indexing
     * in {@link "@tezos-dappetizer/indexer".IndexingDappetizerConfig.contracts}.
     * If an non-empty array is specified, then token indexer is executed only when particular contracts are encountered.
     * Configured contracts must be indexable by token indexer so they must comply to *FA1.2* or *FA2*.
     * @default `undefined`
     */
    contractNames?: NonEmptyArray<string>;

    contractMetadataCacheMillis?: number;
}

export interface TokenIndexerModuleConfig {
    id: '@tezos-dappetizer/token-indexer';
    config?: TokenIndexerConfig;
}
