import { isNullish, isWhiteSpace, Nullish } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { rpcGuard } from '../../helpers/rpc-guard';

@singleton()
export class MetadataSanitizer {
    /** Some metadata contain control chars -> remove them. */
    sanitizeString(value: Nullish<string>): string | null {
        if (isNullish(value)) {
            return null;
        }

        const str = rpcGuard.string(value);
        const printableChars = str.split('').filter(c => c.charCodeAt(0) >= 32);
        return printableChars.join('');
    }

    /** Some metadata contain string or null instead of number -> convert it. */
    sanitizeInteger(value: Nullish<number>): number | null {
        if (isNullish(value) || (typeof value === 'string' && isWhiteSpace(value))) {
            return null;
        }

        const parsed = typeof value === 'string' ? Number(value) : NaN;
        return rpcGuard.integer(!isNaN(parsed) ? parsed : value);
    }
}
