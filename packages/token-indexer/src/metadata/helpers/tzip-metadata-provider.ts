import { MetadataState } from '../../entities/metadata-state';

export interface TzipMetadataProvider<TMetadata extends TzipMetadata, TArgs extends unknown[]> {
    get(...args: TArgs): Promise<TMetadata>;
}

export interface TzipMetadata {
    readonly metadataState: MetadataState;
}
