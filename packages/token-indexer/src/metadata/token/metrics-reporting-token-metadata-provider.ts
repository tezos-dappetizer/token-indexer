import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import { Clock, CLOCK_DI_TOKEN } from '@tezos-dappetizer/utils';
import BigNumber from 'bignumber.js';
import { inject, singleton } from 'tsyringe';

import { TokenMetadata } from './token-metadata';
import { TokenMetadataProvider } from './token-metadata-provider';
import { Tzip12TokenMetadataProvider } from './tzip12-token-metadata-provider';
import { TokenIndexerMetrics } from '../../helpers/token-indexer-metrics';
import { MetricsReportingMetadataProvider } from '../helpers/metrics-reporting-metadata-provider';

@singleton()
export class MetricsReportingTokenMetadataProvider
    extends MetricsReportingMetadataProvider<TokenMetadata, [ContractAbstraction, BigNumber]>
    implements TokenMetadataProvider {
    constructor(
        metrics: TokenIndexerMetrics,
        @inject(Tzip12TokenMetadataProvider) nextProvider: TokenMetadataProvider,
        @inject(CLOCK_DI_TOKEN) clock: Clock,
    ) {
        super(nextProvider, metrics, clock, { feature: 'token' });
    }
}
