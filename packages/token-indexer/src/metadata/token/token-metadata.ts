import { DbToken } from '../../entities/db-token';
import { MetadataState } from '../../entities/metadata-state';

export type TokenMetadata =
    Readonly<Pick<DbToken,
    | 'decimals'
    | 'metadata'
    | 'metadataState'
    | 'name'
    | 'symbol'>>;

export const emptyTokenMetadata: Readonly<Record<MetadataState, TokenMetadata>> = {
    Failed: { metadataState: MetadataState.Failed, decimals: null, metadata: null, name: null, symbol: null },
    Ok: { metadataState: MetadataState.Ok, decimals: null, metadata: null, name: null, symbol: null },
    Pending: { metadataState: MetadataState.Pending, decimals: null, metadata: null, name: null, symbol: null },
};
