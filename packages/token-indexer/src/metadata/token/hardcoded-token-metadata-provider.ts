import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly, StrictOmit } from 'ts-essentials';
import { inject, singleton } from 'tsyringe';

import { MetricsReportingTokenMetadataProvider } from './metrics-reporting-token-metadata-provider';
import { TokenMetadata } from './token-metadata';
import { TokenMetadataProvider } from './token-metadata-provider';
import { MetadataState } from '../../entities/metadata-state';
import { contractAddresses } from '../../helpers/contract-addresses';

@singleton()
export class HardcodedTokenMetadataProvider implements TokenMetadataProvider {
    constructor(@inject(MetricsReportingTokenMetadataProvider) private readonly nextProvider: TokenMetadataProvider) {}

    async get(contract: ContractAbstraction, tokenId: BigNumber): Promise<TokenMetadata> {
        const hardcoded = hardcodedTokens[contract.address]?.[tokenId.toFixed()];
        return hardcoded
            ? {
                ...hardcoded,
                metadataState: MetadataState.Ok,
                metadata: JSON.stringify(hardcoded),
            }
            : this.nextProvider.get(contract, tokenId);
    }
}

const hardcodedTokens: DeepReadonly<Record<string, Record<string, StrictOmit<TokenMetadata, 'metadata' | 'metadataState'>>>> = {
    [contractAddresses.ETH_TZ]: {
        0: { symbol: 'ETHtz', name: 'ETHtez', decimals: 18 },
    },
    [contractAddresses.K_USD]: {
        0: { symbol: 'kUSD', name: 'Kolibri USD', decimals: 18 },
    },
    [contractAddresses.MINTERPOP]: {
        0: { symbol: '', name: 'The Nine (Adriana Melo Cover)', decimals: 0 },
    },
    [contractAddresses.STKR]: {
        0: { symbol: 'STKR', name: 'STKR', decimals: 18 },
    },
    [contractAddresses.TZ_BTC]: {
        0: { symbol: 'tzBTC', name: 'tzBTC', decimals: 8 },
    },
    [contractAddresses.USD_TZ]: {
        0: { symbol: 'USDtz', name: 'USDtez', decimals: 6 },
    },
    [contractAddresses.W_XTZ]: {
        0: { symbol: 'wXTZ', name: 'wXTZ', decimals: 6 },
    },
};
