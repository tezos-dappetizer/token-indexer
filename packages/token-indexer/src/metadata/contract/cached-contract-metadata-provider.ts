import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import NodeCache from 'node-cache';
import { inject, singleton } from 'tsyringe';

import { ContractMetadata } from './contract-metadata';
import { ContractMetadataProvider } from './contract-metadata-provider';
import { HardcodedContractMetadataProvider } from './hardcoded-contract-metadata-provider';
import { TokenIndexerInternalConfig } from '../../config/token-indexer-internal-config';

@singleton()
export class CachedContractMetadataProvider implements ContractMetadataProvider {
    private readonly cache = new NodeCache({ useClones: false });

    constructor(
        @inject(HardcodedContractMetadataProvider) private readonly nextProvider: ContractMetadataProvider,
        private readonly config: TokenIndexerInternalConfig,
    ) {}

    get(contract: ContractAbstraction): Promise<ContractMetadata> { // eslint-disable-line @typescript-eslint/promise-function-async
        let promise = this.cache.get<Promise<ContractMetadata>>(contract.address);
        const cacheSeconds = this.config.contractMetadataCacheMillis / 1000;

        if (!promise) {
            promise = this.nextProvider.get(contract);
            this.cache.set(contract.address, promise, cacheSeconds);
        } else {
            this.cache.ttl(contract.address, cacheSeconds); // Sliding expiration.
        }
        return promise;
    }
}
