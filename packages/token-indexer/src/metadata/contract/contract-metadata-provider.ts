import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import { InjectionToken } from 'tsyringe';

import { ContractMetadata } from './contract-metadata';
import { SkippableContractMetadataProvider } from './skippable-contract-metadata-provider';
import { TzipMetadataProvider } from '../helpers/tzip-metadata-provider';

export type ContractMetadataProvider = TzipMetadataProvider<ContractMetadata, [ContractAbstraction]>;

export const CONTRACT_METADATA_PROVIDER_DI_TOKEN: InjectionToken<ContractMetadataProvider> = SkippableContractMetadataProvider;
