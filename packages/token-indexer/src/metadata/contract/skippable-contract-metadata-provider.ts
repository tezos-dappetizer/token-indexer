import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import { inject, singleton } from 'tsyringe';

import { CachedContractMetadataProvider } from './cached-contract-metadata-provider';
import { ContractMetadata, emptyContractMetadata } from './contract-metadata';
import { ContractMetadataProvider } from './contract-metadata-provider';
import { TokenIndexerInternalConfig } from '../../config/token-indexer-internal-config';

@singleton()
export class SkippableContractMetadataProvider implements ContractMetadataProvider {
    constructor(
        @inject(CachedContractMetadataProvider) private readonly nextProvider: ContractMetadataProvider,
        private readonly config: TokenIndexerInternalConfig,
    ) {}

    async get(contract: ContractAbstraction): Promise<ContractMetadata> {
        return this.config.skipGetMetadata
            ? emptyContractMetadata.Ok
            : this.nextProvider.get(contract);
    }
}
