import * as taquitoTzip16 from '@taquito/tzip16';
import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import {
    Constructor,
    errorToString,
    hasConstructor,
    injectLogger,
    Logger,
    runInPerformanceSection,
} from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { ContractMetadata, emptyContractMetadata } from './contract-metadata';
import { ContractMetadataProvider } from './contract-metadata-provider';
import { MetadataState } from '../../entities/metadata-state';
import { rpcGuard } from '../../helpers/rpc-guard';
import { MetadataSanitizer } from '../helpers/metadata-sanitizer';

const notFoundErrorTypes: readonly Constructor[] = [
    taquitoTzip16.BigMapContractMetadataNotFoundError,
    taquitoTzip16.ContractMetadataNotFoundError,
    taquitoTzip16.UriNotFoundError,
];

@singleton()
export class Tzip16ContractMetadataProvider implements ContractMetadataProvider {
    constructor(
        private readonly sanitizer: MetadataSanitizer,
        @injectLogger(Tzip16ContractMetadataProvider) private readonly logger: Logger,
    ) {}

    async get(contract: ContractAbstraction): Promise<ContractMetadata> {
        try {
            return await runInPerformanceSection(this.logger, async () => {
                const envelope = await contract.tzip16().getMetadata();
                return this.createMetadata(envelope);
            });
        } catch (error) {
            if (notFoundErrorTypes.some(t => hasConstructor(error, t))) {
                this.logger.logDebug('Received not-found response for {contract} hense using empty metadata.', {
                    contract: contract.address,
                });
                return emptyContractMetadata.Ok;
            }

            this.logger.logError('Failed to get TZIP-16 metadata for {contract}. {error}', {
                contract: contract.address,
                error,
            });
            return emptyContractMetadata.Failed;
        }
    }

    private createMetadata({ metadata, uri }: taquitoTzip16.MetadataEnvelope): ContractMetadata {
        try {
            rpcGuard.object(metadata);
            return {
                metadataState: MetadataState.Ok,
                name: this.sanitizer.sanitizeString(metadata.name),
                description: this.sanitizer.sanitizeString(metadata.description),
            };
        } catch (error) {
            const details = errorToString(error, { onlyMessage: true });
            throw new Error(`Failed to deserialize metadata from ${uri} with response: ${JSON.stringify(metadata)}\n${details}`);
        }
    }
}
