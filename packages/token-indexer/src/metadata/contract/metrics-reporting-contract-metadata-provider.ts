import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import { Clock, CLOCK_DI_TOKEN } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { ContractMetadata } from './contract-metadata';
import { ContractMetadataProvider } from './contract-metadata-provider';
import { Tzip16ContractMetadataProvider } from './tzip16-contract-metadata-provider';
import { TokenIndexerMetrics } from '../../helpers/token-indexer-metrics';
import { MetricsReportingMetadataProvider } from '../helpers/metrics-reporting-metadata-provider';

@singleton()
export class MetricsReportingContractMetadataProvider
    extends MetricsReportingMetadataProvider<ContractMetadata, [ContractAbstraction]>
    implements ContractMetadataProvider {
    constructor(
        metrics: TokenIndexerMetrics,
        @inject(Tzip16ContractMetadataProvider) nextProvider: ContractMetadataProvider,
        @inject(CLOCK_DI_TOKEN) clock: Clock,
    ) {
        super(nextProvider, metrics, clock, { feature: 'contract' });
    }
}
