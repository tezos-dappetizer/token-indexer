import { DbContract } from '../../entities/db-contract';
import { MetadataState } from '../../entities/metadata-state';
import { TzipMetadata } from '../helpers/tzip-metadata-provider';

export type ContractMetadata =
    TzipMetadata
    & Readonly<Pick<DbContract,
    | 'description'
    | 'name'>>;

export const emptyContractMetadata: Readonly<Record<MetadataState.Ok | MetadataState.Failed, ContractMetadata>> = {
    Failed: { metadataState: MetadataState.Failed, name: null, description: null },
    Ok: { metadataState: MetadataState.Ok, name: null, description: null },
};
