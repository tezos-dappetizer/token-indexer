import { BigMapUpdateIndexingContext, TransactionIndexingContext } from '@tezos-dappetizer/indexer';

import { DbTokenKey } from './entities/db-token';
import { IndexedChange } from './indexed-data';

export interface TokenIndexingData {
    readonly changes: IndexedChange[];
    readonly keysOfTokensPendingMetadata: DbTokenKey[];
}

export type TokenTransactionIndexingContext<TContractData = unknown> = TransactionIndexingContext<TokenIndexingData, TContractData>;

export type TokenBigMapUpdateIndexingContext<TContractData = unknown> = BigMapUpdateIndexingContext<TokenIndexingData, TContractData>;
