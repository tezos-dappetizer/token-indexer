import { DB_EXECUTOR_DI_TOKEN, DbExecutor, InsertCommand } from '@tezos-dappetizer/database';
import { Block } from '@tezos-dappetizer/indexer';
import { typed } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';
import { EntityManager } from 'typeorm';

import { DbUpdater } from './db-updater';
import { DbAction, DbActionAddresses, DbActionValues } from '../entities/db-action';
import { IndexedChange, IndexedChangeType } from '../indexed-data';
import { TokenIndexingData } from '../token-indexing-data';

@singleton()
export class DbActionsUpdater implements DbUpdater {
    constructor(
        @inject(DB_EXECUTOR_DI_TOKEN) private readonly dbExecutor: DbExecutor,
    ) {}

    async applyChanges(dbManager: EntityManager, data: TokenIndexingData, block: Block): Promise<void> {
        const dbActions = data.changes.map((change, index) => typed<DbActionValues>({
            order: index + 1,
            type: change.type,
            block: { hash: block.hash },
            operationGroupHash: change.operationGroupHash,
            token: {
                contractAddress: change.contract.address,
                id: change.tokenId,
            },
            amount: change.amount,
            ...mapToDbAction(change),
        }));

        if (dbActions.length) {
            await this.dbExecutor.execute(dbManager, new InsertCommand(DbAction, dbActions));
        }
    }
}

function mapToDbAction(change: IndexedChange): DbActionAddresses {
    switch (change.type) {
        case IndexedChangeType.Burn:
            return {
                fromAddress: change.fromAddress,
            };
        case IndexedChangeType.Ledger:
            return {
                ownerAddress: change.ownerAddress,
            };
        case IndexedChangeType.Mint:
            return {
                toAddress: change.toAddress,
            };
        case IndexedChangeType.Transfer:
            return {
                fromAddress: change.fromAddress,
                toAddress: change.toAddress,
            };
    }
}
