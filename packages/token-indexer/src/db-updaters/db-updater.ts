import { Block } from '@tezos-dappetizer/indexer';
import { EntityManager } from 'typeorm';

import { TokenIndexingData } from '../token-indexing-data';

export interface DbUpdater {
    applyChanges(
        dbManager: EntityManager,
        data: TokenIndexingData,
        block: Block,
    ): Promise<void>;
}
