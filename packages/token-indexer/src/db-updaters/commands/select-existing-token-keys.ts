import { DbCommand } from '@tezos-dappetizer/database';
import { dumpToString, getConstructorName, keyof } from '@tezos-dappetizer/utils';
import { EntityManager } from 'typeorm';

import { DbToken, DbTokenKey } from '../../entities/db-token';

export class SelectExistingTokenKeys implements DbCommand<readonly DbTokenKey[]> {
    readonly description: string;

    constructor(
        readonly keysToCheck: readonly DbTokenKey[],
    ) {
        this.description = `${getConstructorName(this)} ${dumpToString(keysToCheck)}`;
    }

    async execute(dbManager: EntityManager): Promise<readonly DbTokenKey[]> {
        return dbManager.createQueryBuilder()
            .select(`t.${keyof<DbToken>('id')}`)
            .addSelect(`t.${keyof<DbToken>('contractAddress')}`)
            .from(DbToken, 't')
            .whereInIds([...this.keysToCheck])
            .getMany();
    }
}
