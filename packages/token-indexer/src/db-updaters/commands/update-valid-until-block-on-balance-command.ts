import { bigNumberDbTransformer, DbCommand } from '@tezos-dappetizer/database';
import { dumpToString, getConstructorName, keyof } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';
import { EntityManager } from 'typeorm';

import { DbBalance } from '../../entities/db-balance';

export type UpdateBalanceFilter = {
    readonly contractAddress: string;
    readonly tokenId: BigNumber;
    readonly ownerAddress?: string;
};

export class UpdateValidUntilBlockOnBalanceCommand implements DbCommand {
    readonly description: string;
    readonly filter: UpdateBalanceFilter;
    readonly validUntilBlockHashToSet: string;

    constructor(options: { filter: UpdateBalanceFilter; validUntilBlockHashToSet: string }) {
        this.filter = options.filter;
        this.validUntilBlockHashToSet = options.validUntilBlockHashToSet;
        this.description = `${getConstructorName(this)} ${dumpToString(options)}`;
    }

    async execute(dbManager: EntityManager): Promise<void> {
        const builder = dbManager.createQueryBuilder()
            .update(DbBalance)
            .where(`${keyof<DbBalance>('validUntilBlockHash')} IS NULL`)
            .andWhere(`${keyof<DbBalance>('tokenContractAddress')} = :contractAddress`, { contractAddress: this.filter.contractAddress })
            .andWhere(`${keyof<DbBalance>('tokenId')} = :tokenId`, { tokenId: bigNumberDbTransformer.to(this.filter.tokenId) });

        if (this.filter.ownerAddress) {
            builder.andWhere(`${keyof<DbBalance>('ownerAddress')} = :ownerAddress`, { ownerAddress: this.filter.ownerAddress });
        }

        builder.set({
            [keyof<DbBalance>('validUntilBlockHash')]: this.validUntilBlockHashToSet,
        });

        await builder.execute();
    }
}
