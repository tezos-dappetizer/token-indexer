import { MichelsonSchema, TransactionParameter } from '@tezos-dappetizer/indexer';
import { NonNullish } from '@tezos-dappetizer/utils';
import { isEqual } from 'lodash';

import { EntrypointProcessor } from './entrypoint-processor';
import { removeAnnots } from '../../helpers/michelson-utils';
import { TokenTransactionIndexingContext } from '../../token-indexing-data';

export abstract class EntrypointProcessorNoAnnots<TRawChange> extends EntrypointProcessor<TRawChange> {
    protected abstract readonly parameterSchemaNoAnnots: MichelsonSchema;

    override shouldProcess(parameterSchema: MichelsonSchema, _address: string): boolean {
        const schemaMichelsonNoAnnots = removeAnnots(parameterSchema.michelson);
        return isEqual(schemaMichelsonNoAnnots, this.parameterSchemaNoAnnots.michelson);
    }

    override deserializeRawChanges(
        transactionParameter: TransactionParameter,
        context: TokenTransactionIndexingContext,
    ): Iterable<TRawChange> {
        const parameter = this.parameterSchemaNoAnnots.execute<NonNullish>(transactionParameter.value.michelson);
        return this.deserializeRawChangesFromNoAnnots(parameter, context);
    }

    protected abstract deserializeRawChangesFromNoAnnots(
        parameter: NonNullish,
        context: TokenTransactionIndexingContext,
    ): Iterable<TRawChange>;
}
