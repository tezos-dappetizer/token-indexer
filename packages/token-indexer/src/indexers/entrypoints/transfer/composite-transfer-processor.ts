import { singleton } from 'tsyringe';

import { Tzip12TransferProcessor } from './processors/tzip12-transfer-processor';
import { Tzip7TransferProcessor } from './processors/tzip7-transfer-processor';
import { RawTransfer } from './transfer-processor-no-annots';
import { CompositeEntrypointProcessor } from '../composite-entrypoint-processor';

@singleton()
export class CompositeTransferProcessor extends CompositeEntrypointProcessor<RawTransfer> {
    constructor() {
        super([
            new Tzip7TransferProcessor(),
            new Tzip12TransferProcessor(),
        ]);
    }
}
