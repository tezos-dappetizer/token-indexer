import { Contract } from '@tezos-dappetizer/indexer';
import { isNullish } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';
import { isEqual } from 'lodash';

import { removeAnnots } from '../../../helpers/michelson-utils';
import { ContractType } from '../../contract-type-resolver';
import { EntrypointProcessorNoAnnots } from '../entrypoint-processor-no-annots';

export abstract class TransferProcessorNoAnnots extends EntrypointProcessorNoAnnots<RawTransfer> {
    static readonly supportedTransferEntrypoint = 'transfer';

    protected abstract readonly contractType: ContractType;

    override resolveContractType(contract: Contract): ContractType | null {
        const parameterSchema = contract.parameterSchemas.entrypoints[TransferProcessorNoAnnots.supportedTransferEntrypoint];
        if (isNullish(parameterSchema)) {
            return null;
        }

        if (isEqual(removeAnnots(parameterSchema.michelson), this.parameterSchemaNoAnnots.michelson)) {
            return this.contractType;
        }

        return null;
    }
}

export interface RawTransfer {
    tokenId: BigNumber;
    fromAddress: string;
    toAddress: string;
    amount: BigNumber;
}
