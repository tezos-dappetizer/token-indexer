import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';

import { contractAddresses } from '../../../../../helpers/contract-addresses';
import { rpcGuard } from '../../../../../helpers/rpc-guard';
import { SINGLE_ASSET_TOKEN_ID } from '../../../../../helpers/token-constants';
import { TokenTransactionIndexingContext } from '../../../../../token-indexing-data';
import { BurnProcessorNoAnnots } from '../../burn-processor-no-annots';
import { RawBurn } from '../../raw-burn';

export class SingleAssetNoFromListBurnProcessor extends BurnProcessorNoAnnots {
    protected override readonly supportedContractAddresses = [contractAddresses.USDS] as const;

    protected override readonly parameterSchemaNoAnnots = new MichelsonSchema({
        prim: 'list',
        args: [{ prim: 'nat' }], // Amount.
    });

    protected override *deserializeRawChangesFromNoAnnots(amounts: BigNumber[], context: TokenTransactionIndexingContext): Iterable<RawBurn> {
        for (const amount of amounts) {
            yield {
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: context.operationWithResult.sourceAddress, // eslint-disable-line deprecation/deprecation
                amount: rpcGuard.positiveBigNumberInteger(amount),
            };
        }
    }
}
