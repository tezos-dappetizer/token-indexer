import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';

import { contractAddresses } from '../../../../../helpers/contract-addresses';
import { rpcGuard } from '../../../../../helpers/rpc-guard';
import { SINGLE_ASSET_TOKEN_ID } from '../../../../../helpers/token-constants';
import { TokenTransactionIndexingContext } from '../../../../../token-indexing-data';
import { BurnProcessorNoAnnots } from '../../burn-processor-no-annots';
import { RawBurn } from '../../raw-burn';

export class PaulBurnProcessor extends BurnProcessorNoAnnots {
    protected override readonly supportedContractAddresses = [contractAddresses.PAUL] as const;

    protected override readonly parameterSchemaNoAnnots = new MichelsonSchema({
        prim: 'pair',
        args: [
            { prim: 'address' },
            { prim: 'nat' },
        ],
    });

    protected override *deserializeRawChangesFromNoAnnots(burn: BurnDto, _context: TokenTransactionIndexingContext): Iterable<RawBurn> {
        yield {
            tokenId: SINGLE_ASSET_TOKEN_ID,
            fromAddress: rpcGuard.string(burn[0]),
            amount: rpcGuard.positiveBigNumberInteger(burn[1]),
        };
    }
}

interface BurnDto {
    '0': string; // From
    '1': BigNumber; // Amount.
}
