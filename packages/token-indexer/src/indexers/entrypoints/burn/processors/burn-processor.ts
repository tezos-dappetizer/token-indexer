import { MichelsonSchema, TransactionParameter } from '@tezos-dappetizer/indexer';
import { NonNullish } from '@tezos-dappetizer/utils';

import { rpcGuard } from '../../../../helpers/rpc-guard';
import { NFT_DEFAULT_AMOUNT, SINGLE_ASSET_TOKEN_ID } from '../../../../helpers/token-constants';
import { TokenTransactionIndexingContext } from '../../../../token-indexing-data';
import { BurnProcessorWithAnnots } from '../burn-processor-with-annots';
import { RawBurn } from '../raw-burn';

export class BurnProcessor extends BurnProcessorWithAnnots {
    override shouldProcess(parameterSchema: MichelsonSchema, _address: string): boolean {
        return this.fromField.existsIn(parameterSchema.generated);
    }

    override *deserializeRawChanges(transactionParameter: TransactionParameter, _context: TokenTransactionIndexingContext): Iterable<RawBurn> {
        const parameters = transactionParameter.value.convert<NonNullish>();

        const from = this.fromField.getValue(parameters);
        const tokenId = this.tokenIdField.getValueOrNull(parameters) ?? SINGLE_ASSET_TOKEN_ID;
        const amount = this.amountField.getValueOrNull(parameters) ?? NFT_DEFAULT_AMOUNT;

        yield {
            tokenId: rpcGuard.positiveBigNumberInteger(tokenId),
            fromAddress: rpcGuard.string(from),
            amount: rpcGuard.positiveBigNumberInteger(amount),
        };
    }
}
