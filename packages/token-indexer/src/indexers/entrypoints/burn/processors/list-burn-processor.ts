import { MichelsonSchema, TransactionParameter } from '@tezos-dappetizer/indexer';
import { NonNullish } from '@tezos-dappetizer/utils';

import { rpcGuard } from '../../../../helpers/rpc-guard';
import { NFT_DEFAULT_AMOUNT, SINGLE_ASSET_TOKEN_ID } from '../../../../helpers/token-constants';
import { TokenTransactionIndexingContext } from '../../../../token-indexing-data';
import { BurnProcessorWithAnnots } from '../burn-processor-with-annots';
import { RawBurn } from '../raw-burn';

export class ListBurnProcessor extends BurnProcessorWithAnnots {
    override shouldProcess(parameterSchema: MichelsonSchema, _address: string): boolean {
        if (parameterSchema.generated.__michelsonType !== 'list') { // eslint-disable-line no-underscore-dangle
            return false;
        }

        const itemSchema = parameterSchema.generated.schema;
        return this.fromField.existsIn(itemSchema);
    }

    override *deserializeRawChanges(transactionParameter: TransactionParameter, _context: TokenTransactionIndexingContext): Iterable<RawBurn> {
        const parameters = transactionParameter.value.convert<NonNullish[]>();

        for (const parameter of parameters) {
            const from = this.fromField.getValue(parameter);
            const amount = this.amountField.getValueOrNull(parameter) ?? NFT_DEFAULT_AMOUNT;
            const tokenId = this.tokenIdField.getValueOrNull(parameter) ?? SINGLE_ASSET_TOKEN_ID;

            yield {
                tokenId: rpcGuard.positiveBigNumberInteger(tokenId),
                fromAddress: rpcGuard.string(from),
                amount: rpcGuard.positiveBigNumberInteger(amount),
            };
        }
    }
}
