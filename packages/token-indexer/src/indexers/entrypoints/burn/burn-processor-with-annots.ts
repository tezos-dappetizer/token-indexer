import BigNumber from 'bignumber.js';

import { RawBurn } from './raw-burn';
import { FieldHandler } from '../../processors-utils';
import { EntrypointProcessor } from '../entrypoint-processor';

export abstract class BurnProcessorWithAnnots extends EntrypointProcessor<RawBurn> {
    protected readonly fromField = new FieldHandler<string>([
        'address',
        'owner',
        'from',
    ]);

    protected readonly amountField = new FieldHandler<BigNumber>([
        'amount',
        'value',
        'token_amount',
    ]);

    protected readonly tokenIdField = new FieldHandler<BigNumber>([
        'token_id',
    ]);
}
