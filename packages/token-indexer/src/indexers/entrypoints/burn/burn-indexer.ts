import { singleton } from 'tsyringe';

import { CompositeBurnProcessor } from './composite-burn-processor';
import { RawBurn } from './raw-burn';
import { TokenIndexerMetrics } from '../../../../src/helpers/token-indexer-metrics';
import { ContractFeature, IndexedChangeType } from '../../../indexed-data';
import { IncompatibleContractReporter } from '../../incompatible-contract-reporter';
import { PartialIndexedChange } from '../../partial-token-indexer';
import { EntrypointIndexer } from '../entrypoint-indexer';

@singleton()
export class BurnIndexer extends EntrypointIndexer<RawBurn> {
    override readonly name = 'burn';

    override readonly contractFeatures = [
        ContractFeature.Burn,
    ] as const;

    override readonly supportedEntrypoints = [
        'burn',
        'burn_token',
        'burn_tokens',
    ] as const;

    constructor(processors: CompositeBurnProcessor, reporter: IncompatibleContractReporter, metrics: TokenIndexerMetrics) {
        super(processors, reporter, metrics);
    }

    override createIndexedChange(rawBurn: RawBurn): PartialIndexedChange {
        return {
            type: IndexedChangeType.Burn,
            tokenId: rawBurn.tokenId,
            fromAddress: rawBurn.fromAddress,
            amount: rawBurn.amount,
        };
    }
}
