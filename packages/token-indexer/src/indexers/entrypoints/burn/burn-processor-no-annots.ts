import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { isEqual } from 'lodash';

import { RawBurn } from './raw-burn';
import { removeAnnots } from '../../../helpers/michelson-utils';
import { EntrypointProcessorNoAnnots } from '../entrypoint-processor-no-annots';

export abstract class BurnProcessorNoAnnots extends EntrypointProcessorNoAnnots<RawBurn> {
    protected abstract readonly supportedContractAddresses: readonly string[];

    override shouldProcess(
        parameterSchema: MichelsonSchema,
        address: string,
    ): boolean {
        const schemaMichelsonNoAnnots = removeAnnots(parameterSchema.michelson);
        return this.supportedContractAddresses.includes(address)
            && isEqual(schemaMichelsonNoAnnots, this.parameterSchemaNoAnnots.michelson);
    }
}
