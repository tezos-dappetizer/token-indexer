import { TransactionParameter, MichelsonSchema, Contract } from '@tezos-dappetizer/indexer';

import { ContractFeature } from '../../indexed-data';
import { TokenTransactionIndexingContext } from '../../token-indexing-data';
import { ContractType, ContractTypeResolver } from '../contract-type-resolver';

export abstract class EntrypointProcessor<TRawChange> implements ContractTypeResolver {
    readonly contractFeatures: readonly ContractFeature[] = [];

    resolveContractType(_contract: Contract): ContractType | null {
        return null;
    }

    abstract shouldProcess(
        parameterSchema: MichelsonSchema,
        address: string,
    ): boolean;

    abstract deserializeRawChanges(
        transactionParameter: TransactionParameter,
        context: TokenTransactionIndexingContext,
    ): Iterable<TRawChange>;
}
