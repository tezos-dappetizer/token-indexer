import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { dumpToString, getConstructorName } from '@tezos-dappetizer/utils';

import { EntrypointProcessor } from './entrypoint-processor';

export abstract class CompositeEntrypointProcessor<TRawChange> {
    constructor(
        readonly processors: readonly EntrypointProcessor<TRawChange>[],
    ) {}

    findProcessor(
        parameterSchema: MichelsonSchema,
        address: string,
    ): EntrypointProcessor<TRawChange> | null {
        const matches = this.processors.filter(p => p.shouldProcess(parameterSchema, address));
        if (matches.length > 1) {
            throw new Error(`${getConstructorName(this)}: multiple entrypoint processors ${dumpToString(matches.map(getConstructorName))}`
                + ` matched for processing schema: ${JSON.stringify(parameterSchema.michelson)}.`);
        }

        return matches[0] ?? null;
    }
}
