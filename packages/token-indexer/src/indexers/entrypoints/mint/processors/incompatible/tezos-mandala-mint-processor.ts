import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';

import { contractAddresses } from '../../../../../helpers/contract-addresses';
import { rpcGuard } from '../../../../../helpers/rpc-guard';
import { NFT_DEFAULT_AMOUNT } from '../../../../../helpers/token-constants';
import { TokenTransactionIndexingContext } from '../../../../../token-indexing-data';
import { MintProcessorNoAnnots } from '../../mint-processor-no-annots';
import { RawMint } from '../../raw-mint';

export class TezosMandalaMintProcessor extends MintProcessorNoAnnots {
    protected override readonly supportedContractAddresses = [contractAddresses.TEZOS_MANDALA] as const;

    protected override readonly parameterSchemaNoAnnots = new MichelsonSchema({ prim: 'nat' });

    protected override *deserializeRawChangesFromNoAnnots(tokenId: BigNumber, context: TokenTransactionIndexingContext): Iterable<RawMint> {
        yield {
            tokenId: rpcGuard.positiveBigNumberInteger(tokenId),
            toAddress: context.operationWithResult.sourceAddress, // eslint-disable-line deprecation/deprecation
            amount: NFT_DEFAULT_AMOUNT,
        };
    }
}
