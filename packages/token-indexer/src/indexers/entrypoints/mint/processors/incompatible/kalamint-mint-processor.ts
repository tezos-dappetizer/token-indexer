import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';

import { contractAddresses } from '../../../../../helpers/contract-addresses';
import { rpcGuard } from '../../../../../helpers/rpc-guard';
import { NFT_DEFAULT_AMOUNT } from '../../../../../helpers/token-constants';
import { TokenTransactionIndexingContext } from '../../../../../token-indexing-data';
import { MintProcessorNoAnnots } from '../../mint-processor-no-annots';
import { RawMint } from '../../raw-mint';

export class KalamintMintProcessor extends MintProcessorNoAnnots {
    protected override readonly supportedContractAddresses = [contractAddresses.KALAMINT] as const;

    protected override readonly parameterSchemaNoAnnots = new MichelsonSchema({
        prim: 'pair',
        args: [
            {
                prim: 'pair',
                args: [
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'string' }, // Category.
                            { prim: 'nat' }, // Collection Id.
                            { prim: 'string' }, // Collection Name.
                        ],
                    },
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'string' }, // Creator Name.
                            { prim: 'nat' }, // Creator Royalty.
                        ],
                    },
                    { prim: 'nat' }, // Editions.
                    { prim: 'string' }, // Ipfs Hash.
                ],
            },
            {
                prim: 'pair',
                args: [
                    { prim: 'string' }, // Keywords.
                    { prim: 'string' }, // Name.
                    { prim: 'bool' }, // On Sale.
                ],
            },
            {
                prim: 'pair',
                args: [
                    { prim: 'mutez' }, // Price.
                    { prim: 'string' }, // Symbol.
                ],
            },
            { prim: 'nat' }, // Token ID.
            { prim: 'bytes' }, // Token Metadata Uri.
        ],
    });

    protected override *deserializeRawChangesFromNoAnnots(mint: MintDto, context: TokenTransactionIndexingContext): Iterable<RawMint> {
        yield {
            tokenId: rpcGuard.positiveBigNumberInteger(mint[12]),
            toAddress: context.operationWithResult.sourceAddress, // eslint-disable-line deprecation/deprecation
            amount: NFT_DEFAULT_AMOUNT,
        };
    }
}

interface MintDto {
    '12': BigNumber; // Token ID.
}
