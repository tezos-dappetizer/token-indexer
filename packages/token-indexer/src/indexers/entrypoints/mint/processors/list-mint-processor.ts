import { MichelsonSchema, TransactionParameter } from '@tezos-dappetizer/indexer';
import { NonNullish } from '@tezos-dappetizer/utils';

import { rpcGuard } from '../../../../helpers/rpc-guard';
import { NFT_DEFAULT_AMOUNT, SINGLE_ASSET_TOKEN_ID } from '../../../../helpers/token-constants';
import { TokenTransactionIndexingContext } from '../../../../token-indexing-data';
import { MintProcessorWithAnnots } from '../mint-processor-with-annots';
import { RawMint } from '../raw-mint';

export class ListMintProcessor extends MintProcessorWithAnnots {
    override shouldProcess(parameterSchema: MichelsonSchema, _address: string): boolean {
        if (parameterSchema.generated.__michelsonType !== 'list') { // eslint-disable-line no-underscore-dangle
            return false;
        }

        const itemSchema = parameterSchema.generated.schema;
        return this.toField.existsIn(itemSchema);
    }

    override *deserializeRawChanges(transactionParameter: TransactionParameter, context: TokenTransactionIndexingContext): Iterable<RawMint> {
        const parameters = transactionParameter.value.convert<NonNullish[]>();

        for (const parameter of parameters) {
            // eslint-disable-next-line deprecation/deprecation
            const to = this.toField.getValueOrNull(parameter) ?? context.operationWithResult.sourceAddress;
            const amount = this.amountField.getValueOrNull(parameter) ?? NFT_DEFAULT_AMOUNT;
            const tokenId = this.tokenIdField.getValueOrNull(parameter) ?? SINGLE_ASSET_TOKEN_ID;

            yield {
                tokenId: rpcGuard.positiveBigNumberInteger(tokenId),
                toAddress: rpcGuard.string(to),
                amount: rpcGuard.positiveBigNumberInteger(amount),
            };
        }
    }
}
