import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { isEqual } from 'lodash';

import { RawMint } from './raw-mint';
import { removeAnnots } from '../../../helpers/michelson-utils';
import { EntrypointProcessorNoAnnots } from '../entrypoint-processor-no-annots';

export abstract class MintProcessorNoAnnots extends EntrypointProcessorNoAnnots<RawMint> {
    protected abstract readonly supportedContractAddresses: readonly string[];

    override shouldProcess(parameterSchema: MichelsonSchema, address: string): boolean {
        const schemaMichelsonNoAnnots = removeAnnots(parameterSchema.michelson);
        return this.supportedContractAddresses.includes(address)
            && isEqual(schemaMichelsonNoAnnots, this.parameterSchemaNoAnnots.michelson);
    }
}
