import { singleton } from 'tsyringe';

import { CompositeMintProcessor } from './composite-mint-processor';
import { RawMint } from './raw-mint';
import { TokenIndexerMetrics } from '../../../../src/helpers/token-indexer-metrics';
import { ContractFeature, IndexedChangeType } from '../../../indexed-data';
import { IncompatibleContractReporter } from '../../incompatible-contract-reporter';
import { PartialIndexedChange } from '../../partial-token-indexer';
import { EntrypointIndexer } from '../entrypoint-indexer';

@singleton()
export class MintIndexer extends EntrypointIndexer<RawMint> {
    override readonly name = 'mint';

    override readonly contractFeatures = [
        ContractFeature.Mint,
    ] as const;

    override readonly supportedEntrypoints = [
        'mint',
        'mint_token',
        'mint_tokens',
    ] as const;

    constructor(processors: CompositeMintProcessor, reporter: IncompatibleContractReporter, metrics: TokenIndexerMetrics) {
        super(processors, reporter, metrics);
    }

    override createIndexedChange(rawMint: RawMint): PartialIndexedChange {
        return {
            type: IndexedChangeType.Mint,
            tokenId: rawMint.tokenId,
            toAddress: rawMint.toAddress,
            amount: rawMint.amount,
        };
    }
}
