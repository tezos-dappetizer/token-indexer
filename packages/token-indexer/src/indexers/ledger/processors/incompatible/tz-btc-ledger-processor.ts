import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';

import { contractAddresses } from '../../../../helpers/contract-addresses';
import { rpcGuard } from '../../../../helpers/rpc-guard';
import { SINGLE_ASSET_TOKEN_ID } from '../../../../helpers/token-constants';
import { ContractFeature } from '../../../../indexed-data';
import { TokenBigMapUpdateIndexingContext } from '../../../../token-indexing-data';
import { unpack } from '../../../processors-utils';
import { RawLedgerUpdate } from '../../ledger-processor';
import { LedgerProcessorNoAnnots } from '../ledger-processor-no-annots';

export class tzBTCLedgerProcessor extends LedgerProcessorNoAnnots {
    override readonly contractFeatures = [
        ContractFeature.SingleAsset,
    ] as const;

    protected override readonly supportedContractAddresses = [contractAddresses.TZ_BTC] as const;

    protected override readonly ledgerBigMapName = '0';

    protected override readonly schemaNoAnnots = {
        key: new MichelsonSchema({
            prim: 'bytes',
        }),
        value: new MichelsonSchema({
            prim: 'bytes',
        }),
    };

    private readonly balanceKeySchema = new MichelsonSchema(
        {
            prim: 'pair',
            args: [
                { prim: 'string' },
                { prim: 'address' },
            ],
        },
    );

    private readonly balanceValueSchema = new MichelsonSchema(
        {
            prim: 'pair',
            args: [
                { prim: 'int' },
                {
                    prim: 'map',
                    args: [
                        { prim: 'address' },
                        { prim: 'nat' },
                    ],
                },
            ],
        },
    );

    private readonly emptyBalanceValue = { 0: new BigNumber(0) };

    protected override *deserializeRawUpdatesFromNoAnnots(
        key: string,
        value: string | null,
        _context: TokenBigMapUpdateIndexingContext,
    ): Iterable<RawLedgerUpdate> {
        const balanceUpdate = this.extractBalanceUpdate(key, value);
        if (balanceUpdate === null) {
            return;
        }

        yield {
            tokenId: SINGLE_ASSET_TOKEN_ID,
            ownerAddress: rpcGuard.string(balanceUpdate.key[1]),
            amount: rpcGuard.positiveBigNumberInteger(balanceUpdate.value[0]),
        };
    }

    private extractBalanceUpdate(key: string, value: string | null): BalanceUpdate | null {
        const keyUnpacked = unpack(key);
        const valueUnpacked = value !== null ? unpack(value) : null;

        let balanceUpdate: BalanceUpdate | null = null;
        try {
            balanceUpdate = {
                key: this.balanceKeySchema.execute(keyUnpacked),
                value: valueUnpacked !== null
                    ? this.balanceValueSchema.execute(valueUnpacked)
                    : this.emptyBalanceValue,
            };
        } catch (error) {
            return null;
        }

        if (balanceUpdate.key['0'] !== 'ledger') {
            return null;
        }

        return balanceUpdate;
    }
}

interface BalanceUpdate {
    key: {
        '0': string;
        '1': string; // Owner.
    };
    value: {
        '0': BigNumber; // Amount.
    };
}
