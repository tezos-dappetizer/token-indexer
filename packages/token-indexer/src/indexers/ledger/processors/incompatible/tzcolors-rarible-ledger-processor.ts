import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { isNullish } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';

import { contractAddresses } from '../../../../helpers/contract-addresses';
import { rpcGuard } from '../../../../helpers/rpc-guard';
import { ContractFeature } from '../../../../indexed-data';
import { TokenBigMapUpdateIndexingContext } from '../../../../token-indexing-data';
import { RawLedgerUpdate } from '../../ledger-processor';
import { LedgerProcessorNoAnnots } from '../ledger-processor-no-annots';

// Related to TzColors (fully annotated) and Rarible (not annotated).
export class TzColorsRaribleLedgerProcessor extends LedgerProcessorNoAnnots {
    override readonly contractFeatures = [
        ContractFeature.Ledger_Nft_like,
    ] as const;

    protected override readonly supportedContractAddresses = [
        contractAddresses.TZ_COLORS,
        contractAddresses.RARIBLE,
    ] as const;

    protected override readonly schemaNoAnnots = {
        key: new MichelsonSchema({
            prim: 'pair',
            args: [
                { prim: 'nat' }, // Token ID.
                { prim: 'address' }, // Owner.
            ],
        }),
        value: new MichelsonSchema({
            prim: 'nat', // Amount.
        }),
    };

    protected override *deserializeRawUpdatesFromNoAnnots(
        key: LedgerKey,
        value: BigNumber | null,
        _context: TokenBigMapUpdateIndexingContext,
    ): Iterable<RawLedgerUpdate> {
        yield {
            tokenId: rpcGuard.positiveBigNumberInteger(key[0]),
            ownerAddress: rpcGuard.string(key[1]),
            amount: !isNullish(value) ? rpcGuard.positiveBigNumberInteger(value) : null,
        };
    }
}

interface LedgerKey {
    '0': BigNumber; // Token ID.
    '1': string; // Owner.
}
