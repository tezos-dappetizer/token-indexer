import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { isNullish } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';

import { LedgerProcessorCompatible } from './ledger-processor-compatible';
import { rpcGuard } from '../../../helpers/rpc-guard';
import { NFT_DEFAULT_AMOUNT } from '../../../helpers/token-constants';
import { ContractFeature } from '../../../indexed-data';
import { TokenBigMapUpdateIndexingContext } from '../../../token-indexing-data';
import { ContractType } from '../../contract-type-resolver';
import { RawLedgerUpdate } from '../ledger-processor';

export class Tzip12NftLedgerProcessor extends LedgerProcessorCompatible {
    override readonly contractFeatures = [
        ContractFeature.Ledger_Tzip12,
        ContractFeature.Ledger_Nft_like,
    ] as const;

    protected override readonly contractType = ContractType.FA2_like;

    protected override readonly schemaNoAnnots = {
        key: new MichelsonSchema({
            prim: 'nat', // Token ID.
        }),
        value: new MichelsonSchema({
            prim: 'address', // Owner.
        }),
    };

    override *deserializeRawUpdatesFromNoAnnots(
        key: BigNumber,
        value: string | null,
        _context: TokenBigMapUpdateIndexingContext,
    ): Iterable<RawLedgerUpdate> {
        yield {
            tokenId: rpcGuard.positiveBigNumberInteger(key),
            ownerAddress: !isNullish(value) ? rpcGuard.string(value) : null,
            amount: !isNullish(value) ? NFT_DEFAULT_AMOUNT : null,
        };
    }
}
