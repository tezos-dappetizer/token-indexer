import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { isNullish } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';

import { LedgerProcessorCompatible } from './ledger-processor-compatible';
import { rpcGuard } from '../../../helpers/rpc-guard';
import { ContractFeature } from '../../../indexed-data';
import { TokenBigMapUpdateIndexingContext } from '../../../token-indexing-data';
import { ContractType } from '../../contract-type-resolver';
import { RawLedgerUpdate } from '../ledger-processor';

export class Tzip12MultiAssetLedgerProcessor extends LedgerProcessorCompatible {
    override readonly contractFeatures = [
        ContractFeature.Ledger_Tzip12,
        ContractFeature.MultiAsset,
    ] as const;

    protected override readonly contractType = ContractType.FA2_like;

    protected override readonly schemaNoAnnots = {
        key: new MichelsonSchema({
            prim: 'pair',
            args: [
                { prim: 'address' }, // Owner.
                { prim: 'nat' }, // Token ID.
            ],
        }),
        value: new MichelsonSchema({
            prim: 'nat', // Amount.
        }),
    };

    override *deserializeRawUpdatesFromNoAnnots(
        key: LedgerKey,
        value: BigNumber | null,
        _context: TokenBigMapUpdateIndexingContext,
    ): Iterable<RawLedgerUpdate> {
        yield {
            tokenId: rpcGuard.positiveBigNumberInteger(key[1]),
            ownerAddress: rpcGuard.string(key[0]),
            amount: !isNullish(value) ? rpcGuard.positiveBigNumberInteger(value) : null,
        };
    }
}

export interface LedgerKey {
    '0': string; // Owner.
    '1': BigNumber; // Token ID.
}
