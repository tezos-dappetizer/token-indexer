import { BigMapInfo, LazyMichelsonValue, MichelsonSchema } from '@tezos-dappetizer/indexer';
import { NonNullish } from '@tezos-dappetizer/utils';
import { isEqual } from 'lodash';

import { removeAnnots } from '../../../helpers/michelson-utils';
import { TokenBigMapUpdateIndexingContext } from '../../../token-indexing-data';
import { LedgerProcessor, RawLedgerUpdate } from '../ledger-processor';

export abstract class LedgerProcessorNoAnnots extends LedgerProcessor {
    protected readonly supportedContractAddresses: readonly string[] = [];
    protected readonly ledgerBigMapName: string = 'ledger';

    protected abstract readonly schemaNoAnnots: {
        readonly key: MichelsonSchema;
        readonly value: MichelsonSchema;
    };

    override shouldProcess(bigMap: BigMapInfo, address: string): boolean {
        return (this.supportedContractAddresses.length === 0 || this.supportedContractAddresses.includes(address))
            && bigMap.name === this.ledgerBigMapName
            && isEqual(removeAnnots(bigMap.keySchema.michelson), this.schemaNoAnnots.key.michelson)
            && isEqual(removeAnnots(bigMap.valueSchema.michelson), this.schemaNoAnnots.value.michelson);
    }

    override *deserializeRawUpdates(
        key: LazyMichelsonValue,
        value: LazyMichelsonValue | null,
        context: TokenBigMapUpdateIndexingContext,
    ): Iterable<RawLedgerUpdate> {
        const convertedKey = this.schemaNoAnnots.key.execute<NonNullish>(key.michelson);
        const convertedValue = value ? this.schemaNoAnnots.value.execute<NonNullish>(value.michelson) : null;

        for (const update of this.deserializeRawUpdatesFromNoAnnots(convertedKey, convertedValue, context)) {
            yield update;
        }
    }

    protected abstract deserializeRawUpdatesFromNoAnnots(
        key: NonNullish,
        value: NonNullish | null,
        context: TokenBigMapUpdateIndexingContext,
    ): Iterable<RawLedgerUpdate>;
}
