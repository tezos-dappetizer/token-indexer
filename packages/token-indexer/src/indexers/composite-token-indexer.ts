import { ContractIndexerUsingDb, DbContext } from '@tezos-dappetizer/database';
import { BigMapUpdate, ContractIndexingContext, LazyContract, TransactionParameter } from '@tezos-dappetizer/indexer';
import { dumpToString, getConstructorName } from '@tezos-dappetizer/utils';
import { groupBy, isEmpty } from 'lodash';
import { noop } from 'ts-essentials';
import { inject, injectAll, singleton } from 'tsyringe';

import { CompositeContractTypeResolver } from './composite-contract-type-resolver';
import { PARTIAL_TOKEN_INDEXERS_DI_TOKEN, PartialIndexedChange, PartialTokenIndexer } from './partial-token-indexer';
import { TokenIndexerInternalConfig } from '../config/token-indexer-internal-config';
import { ContractFeature, IndexedContract } from '../indexed-data';
import { ContractMetadataProvider, CONTRACT_METADATA_PROVIDER_DI_TOKEN } from '../metadata/contract/contract-metadata-provider';
import {
    TokenBigMapUpdateIndexingContext,
    TokenIndexingData,
    TokenTransactionIndexingContext,
} from '../token-indexing-data';

@singleton()
export class CompositeTokenIndexer implements Required<ContractIndexerUsingDb<TokenIndexingData, CompositeContractData>> {
    readonly name = getConstructorName(this);

    // Empty methods to fully implement the interface.
    readonly indexOrigination = noop;
    readonly indexStorageChange = noop;
    readonly indexBigMapDiff = noop;
    readonly indexEvent = noop;

    constructor(
        @injectAll(PARTIAL_TOKEN_INDEXERS_DI_TOKEN) private readonly tokenIndexers: readonly PartialTokenIndexer[],
        @inject(CONTRACT_METADATA_PROVIDER_DI_TOKEN) private readonly contractMetadataProvider: ContractMetadataProvider,
        private readonly contractTypeResolver: CompositeContractTypeResolver,
        private readonly config: TokenIndexerInternalConfig,
    ) {
        const duplicate = Object.entries(groupBy(tokenIndexers, i => i.name)).find(g => g[1].length > 1);
        if (duplicate) {
            throw new Error(`Name '${duplicate[0]}' is used for multiple PartialTokenIndexer-s:`
                + ` ${dumpToString(duplicate[1].map(getConstructorName))}.`);
        }
    }

    async shouldIndex(lazyContract: LazyContract): Promise<CompositeContractData | false> {
        if (!this.isConfiguredToBeIndexed(lazyContract)) {
            return false;
        }

        const contract = await lazyContract.getContract();
        const contractType = this.contractTypeResolver.resolveContractType(contract);
        if (contractType === null) {
            return false;
        }

        const features: ContractFeature[] = [ContractFeature[contractType]];
        const partialData: Record<string, unknown> = {};
        for (const indexer of this.tokenIndexers) {
            const result = indexer.shouldIndex(contract);

            if (result !== false) {
                features.push(...result.contractFeatures);
                partialData[indexer.name] = result.data;
            }
        }

        if (isEmpty(partialData)) {
            throw new Error(`Failed to find processors for contract '${contract.address}' with resolved type' ${contractType}'.`);
        }

        const metadata = await this.contractMetadataProvider.get(contract.abstraction);
        return {
            tokenIndexersData: partialData,
            indexedContract: {
                address: contract.address,
                features,
                metadata,
            },
        };
    }

    indexTransaction(
        transactionParameter: TransactionParameter,
        _dbContext: DbContext,
        context: TokenTransactionIndexingContext<CompositeContractData>,
    ): void {
        this.runIndexing(context, (indexer, contractData) => indexer.indexTransaction?.(transactionParameter, { ...context, contractData }));
    }

    indexBigMapUpdate(
        bigMapUpdate: BigMapUpdate,
        _dbContext: DbContext,
        context: TokenBigMapUpdateIndexingContext<CompositeContractData>,
    ): void {
        this.runIndexing(context, (indexer, contractData) => indexer.indexBigMapUpdate?.(bigMapUpdate, { ...context, contractData }));
    }

    private runIndexing(
        context: ContractIndexingContext<TokenIndexingData, CompositeContractData>,
        iterateChanges: (tokenIndexer: PartialTokenIndexer, contractData: unknown) => Iterable<PartialIndexedChange> | undefined,
    ): void {
        for (const tokenIndexer of this.tokenIndexers) {
            if (!(tokenIndexer.name in context.contractData.tokenIndexersData)) {
                continue;
            }

            const contractData = context.contractData.tokenIndexersData[tokenIndexer.name];
            const partialChanges = iterateChanges(tokenIndexer, contractData);

            for (const partialChange of partialChanges ?? []) {
                context.data.changes.push({
                    ...partialChange,
                    contract: context.contractData.indexedContract,
                    operationGroupHash: context.operationGroup.hash,
                });
            }
        }
    }

    private isConfiguredToBeIndexed(lazyContract: LazyContract): boolean {
        if (!this.config.contractNames) {
            return true; // TokenIndexer selective indexing disabled.
        }
        if (!lazyContract.config.name) {
            return false; // Unknown contract.
        }
        return this.config.contractNames.includes(lazyContract.config.name);
    }
}

export interface CompositeContractData {
    readonly indexedContract: IndexedContract;
    readonly tokenIndexersData: Readonly<Record<string, unknown>>;
}
