import { BigNumber } from 'bignumber.js';

import { ContractMetadata } from './metadata/contract/contract-metadata';

export type IndexedChange =
    LedgerUpdateChange
    | TransferTokensChange
    | MintTokensChange
    | BurnTokensChange;

export enum IndexedChangeType {
    Ledger = 'Ledger',
    Transfer = 'Transfer',
    Mint = 'Mint',
    Burn = 'Burn',
}

export interface IndexedContract {
    readonly address: string;
    readonly features: readonly ContractFeature[];
    readonly metadata: ContractMetadata;
}

export interface IndexedChangeBase {
    readonly contract: IndexedContract;
    readonly operationGroupHash: string;
    readonly tokenId: BigNumber;
}

export interface LedgerUpdateChange extends IndexedChangeBase {
    readonly type: IndexedChangeType.Ledger;
    readonly ownerAddress: string | null; // Null is a removal of NFT.
    readonly amount: BigNumber | null; // Null is a removal.
}

export interface TransferTokensChange extends IndexedChangeBase {
    readonly type: IndexedChangeType.Transfer;
    readonly fromAddress: string;
    readonly toAddress: string;
    readonly amount: BigNumber;
}

export interface MintTokensChange extends IndexedChangeBase {
    readonly type: IndexedChangeType.Mint;
    readonly toAddress: string;
    readonly amount: BigNumber;
}

export interface BurnTokensChange extends IndexedChangeBase {
    readonly type: IndexedChangeType.Burn;
    readonly fromAddress: string;
    readonly amount: BigNumber;
}

export enum ContractFeature {
    FA1_2 = 'FA1_2', // The contract strictly implements standard TZIP-7 - has transfer method according to the standard.
    FA2 = 'FA2', // The contract strictly implements standard TZIP-12 - has transfer method according to the standard.

    FA1_2_like = 'FA1_2_like', // See `ContractType`.
    FA2_like = 'FA2_like', // See `ContractType`.

    Ledger = 'Ledger', // The contract has a ledger in its storage according to one of supported formats.
    Ledger_Tzip7 = 'Ledger_Tzip7', // The ledger is according to TZIP-7.
    Ledger_Tzip12 = 'Ledger_Tzip12', // The ledger is according to TZIP-12.

    SingleAsset = 'SingleAsset', // The ledger is of a single-asset contract according to TZIP-7 or TZIP-12.
    MultiAsset = 'MultiAsset', // The ledger is of a multi-asset contract according to TZIP-12.
    Ledger_Nft_like = 'Ledger_Nft_like', // The ledger is of a NFT style contract.

    Transfer = 'Transfer', // The contract has a transfer entrypoint according to one of supported formats.
    Transfer_Tzip7 = 'Transfer_Tzip7', // The contract has a transfer entrypoint according to TZIP-7.
    Transfer_Tzip12 = 'Transfer_Tzip12', // The contract has a transfer entrypoint according to TZIP-12.

    Mint = 'Mint', // The contract has a mint entrypoint according to one of supported formats.
    Burn = 'Burn', // The contract has a burn entrypoint according to one of supported formats.
}
