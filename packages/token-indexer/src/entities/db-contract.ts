import { commonDbColumns, DbBlock } from '@tezos-dappetizer/database';
import { MergeN, StrictOmit } from 'ts-essentials';
import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';

import { DbBlockKey } from './db-block-key';
import { MetadataState, metadataStateColumnOptions } from './metadata-state';
import { PickChangingType } from '../helpers/utils';

export const contractAddressOptions = commonDbColumns.address;

@Entity()
export class DbContract {
    @PrimaryColumn(contractAddressOptions)
        address!: string;

    @Column({ type: 'varchar', nullable: true })
        name!: string | null;

    @Column({ type: 'varchar', nullable: true })
        description!: string | null;

    @Column(metadataStateColumnOptions)
        metadataState!: MetadataState;

    @Column(commonDbColumns.operationGroupHash)
        firstOperationGroupHash!: string;

    // First block relation:
    @ManyToOne(() => DbBlock, { onDelete: 'CASCADE' })
        firstBlock!: DbBlock;

    @Column(commonDbColumns.blockHash)
        firstBlockHash!: string;
}

export type DbContractKey = Pick<DbContract, 'address'>;

export type DbContractValues = MergeN<[
    StrictOmit<DbContract, 'firstBlockHash'>,
    PickChangingType<DbContract, 'firstBlock', DbBlockKey>,
]>;
