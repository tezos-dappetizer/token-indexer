import { dbColumnName, DbInitializer, dbTableName, resolveSchema } from '@tezos-dappetizer/database';
import { getConstructorName, injectLogger, Logger } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';
import { DataSource } from 'typeorm';

import { DbBalance } from '../entities/db-balance';

const CURRENT_BALANCE_VIEW_NAME = 'current_balance';

@singleton()
export class TokenIndexerDbInitializer implements DbInitializer {
    readonly name = getConstructorName(this);

    constructor(
        @injectLogger(TokenIndexerDbInitializer) private readonly logger: Logger,
    ) {}

    async beforeSynchronization(dataSource: DataSource): Promise<void> {
        const view = getFullIdentifier(CURRENT_BALANCE_VIEW_NAME, dataSource);

        this.logger.logInformation('Dropping {view} if it does exist.', { view });
        await dataSource.query(`DROP VIEW IF EXISTS ${view}`);
        this.logger.logInformation('The {view} has been dropped.', { view });
    }

    async afterSynchronization(dataSource: DataSource): Promise<void> {
        const view = getFullIdentifier(CURRENT_BALANCE_VIEW_NAME, dataSource);
        const querySql = `CREATE VIEW ${view} AS 
            SELECT 
                ${dbColumnName<DbBalance>('ownerAddress')}, 
                ${dbColumnName<DbBalance>('amount')}, 
                ${dbColumnName<DbBalance>('operationGroupHash')}, 
                ${dbColumnName<DbBalance>('tokenId')}, 
                ${dbColumnName<DbBalance>('tokenContractAddress')}, 
                ${dbColumnName<DbBalance>('validFromBlockHash')}
            FROM ${getFullIdentifier(dbTableName(DbBalance), dataSource)} 
            WHERE ${dbColumnName<DbBalance>('validUntilBlockHash')} IS NULL;`;

        this.logger.logInformation('Creating view {view}.', { view });
        await dataSource.query(querySql);
        this.logger.logInformation('The {view} has been created.', { view });
    }
}

function getFullIdentifier(entityName: string, dataSource: DataSource): string {
    const schema = resolveSchema(dataSource.options);
    return schema ? `${schema}.${entityName}` : entityName;
}
