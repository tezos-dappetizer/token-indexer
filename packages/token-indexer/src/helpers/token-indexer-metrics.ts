import { asRequiredLabels } from '@tezos-dappetizer/utils';
import { Counter, Gauge, Histogram } from 'prom-client';
import { singleton } from 'tsyringe';

const getMetadataLabels = ['feature'] as const;

/** Container with Prometheus metric objects. */
@singleton()
export class TokenIndexerMetrics {
    readonly processorUsage = asRequiredLabels(new Counter({
        name: 'ai_token_indexer_processor_usage',
        help: 'Number of processor used.',
        labelNames: ['name', 'feature'] as const,
    }));

    readonly tokenMetadataUpdating = new Gauge({
        name: 'ai_token_indexer_token_metadata_updating',
        help: 'Number of token metadata updates (download & write to DB) on the background currently in progress or queued.',
    });

    readonly getMetadataCallDuration = asRequiredLabels(new Histogram({
        name: 'ai_metadata_request_call_duration',
        help: 'How long it took to retrieve metadata (ms).',
        labelNames: getMetadataLabels,
        buckets: [50, 100, 250, 500, 750, 1000, 2000, 5000],
    }));

    readonly getMetadataOkCount = asRequiredLabels(new Counter({
        name: 'ai_metadata_requests_ok',
        help: 'Number of get metadata requests with OK response.',
        labelNames: getMetadataLabels,
    }));

    readonly getMetadataFailedCount = asRequiredLabels(new Counter({
        name: 'ai_metadata_requests_failed',
        help: 'Number of get metadata with failed (not OK) response.',
        labelNames: getMetadataLabels,
    }));
}
