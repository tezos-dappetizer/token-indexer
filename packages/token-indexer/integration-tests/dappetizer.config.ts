import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';

const config: DappetizerConfigUsingDb = {
    modules: [
        { id: '@tezos-dappetizer/token-indexer' },
    ],
    networks: {
        mainnet: {
            indexing: {
                fromBlockLevel: 1875165,
                toBlockLevel: 1875166,
            },
            tezosNode: {
                url: process.env.TEZOS_NODE_URL || 'https://prod.tcinfra.net/rpc/mainnet/',
            },
        },
    },
    ipfs: {
        gateways: [
            'https://ipfs.io/',
            'https://cloudflare-ipfs.com/',
            ...process.env.IPFS_GATEWAY ? [process.env.IPFS_GATEWAY] : [],
        ],
    },
    database: {
        type: 'postgres',
        host: process.env.DB_HOST || 'localhost',
        port: parseInt(process.env.DB_PORT ?? '') || 5432,
        username: process.env.DB_USERNAME || 'postgres',
        password: process.env.DB_PASSWORD || 'postgrespassword',
        database: process.env.DB_DATABASE || 'postgres',
        schema: process.env.DB_SCHEMA || 'token_indexer_int_tests',
        connectTimeoutMS: 1_000,
    },
    usageStatistics: {
        enabled: false,
    },
};

export default config;
