# Story: Indexing of Contracts

**As** a Token Indexer user,  
**When** indexing a block  
**I want** Token Indexer to collect respective contracts,  
**So that** I can use them further.

## Scenario: Indexing Blocks from 1875165 to 1875166

**When** Token Indexer indexes blocks from 1875165 to 1875166  
**Then** Token Indexer collects following contracts:

| Address | Tzip 16 Name | Tzip 16 Description | Operation Group Hash |
| --- | --- | --- | --- |
| KT18fp5rcTW7mbWDmzFwjLDUhs5MeJmagDSZ | Wrap protocol FA2 tokens |  | onvpKU2pqCaPLDMYSF3FEHduac7yM2rC2VWy4n63WQKMMdPkAH5 |
| KT1AFA2mwNUMNd4SsujE1YYp29vd8BZejyKW | hDAO | Hic et nunc DAO FA2 Token | opJzi1yGQ6uRkBZXdfCeBV9TMPRXCVUk1y7izYQ5av2YfHJc1rf |
| KT1Ak8AFA54waFVMyPXNE925cUaQjFEqxuYN | HOLLOWS |  | oo5vpttMmtfLHeVDc3rzdijaD9XMtqME97Kyk5qVukfYLQSN7SE |
| KT1DUpMhmqjJZZo58dMqmKufGbFrZJBvzpkX | Art Skool | A deeper dive into my creative process. This collection features, sketches, case studies, experimental pieces &  videos showing the time lapse painting process. | ooHJ3XKzzYzVdj1VgrB2pww4vZih4939vjz9chbMgFvm8GyWStP |
| KT1E8CrG6uznYAG9vZVGtApMJwwTScxPEUKq | PLENTY DeFi AMM | Plenty AMM Liquidity Provider Token Contract | opWNKpWXxv7vYmXLwKakj2bKx9ny3JMjYbM4FmWnfJ91AKyWw3Q |
| KT1GRSvLoikDsXujKgZPsGLX8k8VvR2Tq95b | PLENTY | Plenty DeFi DAO | ooqtph98j72juYT1ueY2xMbNTii6N5JzTBkWTt7XuAiMWrAS6zX |
| KT1J7pmSsdwpxfa3f9uQuwUKhi8qy3mQW5gU | screentime | video synthesized onto vhs and viewed on a crt | oocrZyVexgtowom6ZY874aMVg7xRUJjvHQCuCDhEQAVopaX4kve |
| KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV | Kolibri Token Contract | FA1.2 Implementation of kUSD | onuHbmuN7QkWfTPw5XpWLwbB288XjkZDpqjTKmwWffCr7FgiQAq |
| KT1KEa8z6vWXDJrVqtMrAeDVzsvxat3kHaCE | FXHASH GENTK | FXHASH contract handling the tokens generated from Generative Tokens. Implements FA2 specs | op4Ze2K2vebMsB3RJFneiDWyUxysYRczvBZkLJELu5vHCT6gfjf |
| KT1LL4HvGebpNriz4nj8wtTzP1QHhktRVo8z | DOS THRASH | A SERIES MADE IN DEDICATION TO TWO MEMETIC ART PHENOMENONS. ONE BEING ROBNESSV2'S DOS TRASH COLLECTION AND THE SECOND BEING A TIMELESS ART PROCESS, DERIVATIVE CREATION, ADVOCATING FOR CRYPTOPHUNKS TODAY AND MANY ARTISTIC CREATIONS BEFORE AND AFTER | op8PhnqyD8XrucH8QBtBfJDb2fUNaCJvMWedxg72giHYHQDZvso |
| KT1PvehkyHwypGNnWSyGnkSfsSjekjWDvxQi | Scenes of island life | This collection of paintings depict ordinary life on St Maarten. Some have a bit of nostalgia because in the pursuit of so-called progress, people tend to shun the very things that make the island unique and make people want to travel there. | ooxfZ6VKKRCRbyGvGubZj9Zni11VZHRV1dCfErqcsSiASLVjMs2 |
| KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | OBJKTs | OBJKTs FA2 collectibles | oopUdfY12U82haWtTVam81PkGPcsXJxcgpCc3A22hZAbFNqQoFN |
| KT1Rpviewjg82JgjGfAKFneSupjAR1kUhbza | xPLENTY | Flash Loan Resistant Governance Token for Plenty DeFi DAO | ooqtph98j72juYT1ueY2xMbNTii6N5JzTBkWTt7XuAiMWrAS6zX |
| KT1ViVwoVfGSCsDaxjwoovejm1aYSGz7s2TZ | Tezotopia NFT Registry | NFT Registry of Tezotopia by Gif.games | op3KUkb6hHWARo29L6qQxUjVWg6QLNcY1QD9mniHxU8defPZRw4 |
| KT1WGDVRnff4rmGzJUbdCRAJBmYt12BrPzdD | PixelPotus | PixelPotus is a digital collectable NFT competition. Each POTUS is created as a series, with 6 different rarities for each and custom artwork for each president at each level. Each higher rarity level token can only be minted by collecting and then burning multiples of lower rarity tokens. The highest level rarity for each POTUS is a Unique NFT. | op1WcfxbniH6w4osJnrhTyptuxfFKk7c5jmGy7wA1k3n2VcDeUi |
| KT1XHyDd8ScUteYRWNocHgwZtj2BwBcoCZZo | CUBE HEADs | CUBE HEAD is a collection of collectibles, each made with a unique cube character set | onmBD4cvv1BT5WBpfZFnCRe3X1h7SLMnoZYdnWYY7YBi4EnG9a8 |
| KT1Xobej4mc6XgEjDoJoHtTKgbD1ELMvcQuL |  |  | ooDTXyNLKf5f1fzDJsLKXx2yEokdbZDTfqf36neC8nfVjuiooEq |
| KT1XRPEPXbZK25r3Htzp2o1x7xdMMmfocKNW |  |  | onvpKU2pqCaPLDMYSF3FEHduac7yM2rC2VWy4n63WQKMMdPkAH5 |