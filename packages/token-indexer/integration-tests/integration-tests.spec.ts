import { DappetizerConfigUsingDb, resolveSchema } from '@tezos-dappetizer/database';
import fs from 'fs';
import { startCase } from 'lodash';
import { npx } from 'node-npx';
import { EOL } from 'os';
import path from 'path';
import { register } from 'ts-node';
import { DataSource } from 'typeorm';

const paths = {
    configFile: `${__dirname}/dappetizer.config.ts`,
    expectedDataDir: `${__dirname}/expected-data`,
    receivedDataDir: `${__dirname}/.tmp-received-data`,
};

describe('integration tests', () => {
    let dbSchema: string;
    let db: DataSource;
    let dappetizerConfig: DappetizerConfigUsingDb;

    beforeAll(async () => {
        dappetizerConfig = loadDappetizerConfig();
        dbSchema = extractSchema(dappetizerConfig);
        db = await new DataSource(dappetizerConfig.database).initialize();
        await ensureNoSchema(db, dbSchema);
        deleteFileOrDir(paths.receivedDataDir);

        await runDappetizer();
    });

    afterAll(async () => {
        await ensureNoSchema(db, dbSchema);
        await db.destroy();
    });

    it('should index basic info about blocks', async () => {
        const blocks = await db.query(`
            SELECT *
            FROM ${dbSchema}.block
            ORDER BY level`);

        expectToEqualFileData(blocks, 'blocks.json');
    });

    it('should index all contracts from the blocks', async () => {
        const contracts: Record<string, string>[] = await db.query(`
            SELECT *
            FROM ${dbSchema}.contract
            ORDER BY address`);

        expectToEqualFileData(contracts, 'contracts.json');
        expectBdd(dappetizerConfig, 'contracts', contracts.map(contract => ({
            address: contract.address,
            tzip16Name: contract.name,
            tzip16Description: contract.description,
            operationGroupHash: contract.first_operation_group_hash,
        })));
    });

    it('should index features of the contracts', async () => {
        const contractFeatures: any[] = await db.query(`SELECT *
            FROM ${dbSchema}.contract_feature
            ORDER BY contract_address, feature`);

        expectToEqualFileData(contractFeatures, 'contract-features.json');

        // We should test all features.
        expect(contractFeatures.map(f => f.feature)).toIncludeAllMembers([
            'FA1_2',
            'FA2',
            'Ledger',
            'Transfer',
            'Mint',
            'Burn',
        ]);
    });

    it('should index all tokens from the blocks', async () => {
        const tokens: Record<string, string>[] = await db.query(`
            SELECT *
            FROM ${dbSchema}.token
            ORDER BY contract_address, id`);

        expectToEqualFileData(tokens, 'tokens.json');
        expectBdd(dappetizerConfig, 'tokens', tokens.map(token => ({
            id: token.id,
            contractAddress: token.contract_address,
            tzip12Name: token.name,
            tzip12Symbol: token.symbol,
            operationGroupHash: token.first_operation_group_hash,
        })));
    });

    it('should index all balances of tokens from the blocks', async () => {
        const balances: Record<string, string>[] = await db.query(`
            SELECT *
            FROM ${dbSchema}.balance
            ORDER BY owner_address, token_contract_address, token_id`);

        expectToEqualFileData(balances, 'balances.json');

        // We should test a balance change.
        const balanceV1 = balances.find(b => typeof b.valid_until_block_hash === 'string');
        expect(balanceV1).toBeDefined();

        const balanceV2 = balances.find(b => b.owner_address === balanceV1!.owner_address
            && b.token_contract_address === balanceV1!.token_contract_address
            && b.token_id === balanceV1!.token_id
            && b.valid_from_block_hash === balanceV1!.valid_until_block_hash);
        expect(balanceV2).toBeDefined();
        expect(balanceV2!.amount).not.toBe(balanceV1!.amount);
        expect(balanceV2!.valid_until_block_hash).toBeNull();

        expectBdd(dappetizerConfig, 'tokenBalances', balances.map(balance => ({
            amount: balance.amount,
            ownerAddress: balance.owner_address,
            tokenId: balance.token_id,
            contractAddress: balance.token_contract_address,
            operationGroupHash: balance.operation_group_hash,
        })));
    });

    it('should index all types of actions from the blocks', async () => {
        const actions: Record<string, string>[] = await db.query(`
            SELECT a.*
            FROM ${dbSchema}.action a
            JOIN ${dbSchema}.block b ON b.hash = a.block_hash
            ORDER BY b.level, a.order`);

        expectToEqualFileData(actions, 'actions.json');

        // We should test all action types.
        expect(actions.map(a => a.type)).toIncludeAllMembers([
            'Burn',
            'Ledger',
            'Mint',
            'Transfer',
        ]);

        expectBdd(dappetizerConfig, 'actions', actions.map(action => ({
            type: action.type,
            tokenId: action.token_id,
            contractAddress: action.token_contract_address,
            operationGroupHash: action.operation_group_hash,
        })));
    });
});

function loadDappetizerConfig(): DappetizerConfigUsingDb {
    register({ compilerOptions: { module: 'CommonJS' } }).enabled(true);
    return require(paths.configFile).default; // eslint-disable-line
}

function extractSchema(dappetizerConfig: DappetizerConfigUsingDb): string {
    const schema = resolveSchema(dappetizerConfig.database);
    if (!schema) {
        throw new Error(`Missing schema in DB config: ${JSON.stringify(dappetizerConfig.database)}`);
    }
    return schema;
}

async function ensureNoSchema(db: DataSource, dbSchema: string): Promise<void> {
    await db.query(`DROP SCHEMA IF EXISTS ${dbSchema} CASCADE`);
}

async function runDappetizer(): Promise<void> {
    const childProcess = npx('dappetizer', ['start', paths.configFile]);
    childProcess.stdout!.pipe(process.stdout);
    childProcess.stderr!.pipe(process.stderr);

    await new Promise<void>((resolve, reject) => {
        childProcess.on('close', exitCode => {
            if (exitCode === 0) {
                resolve();
            } else {
                reject(new Error(`Dappetizer exited with code ${exitCode}.`));
            }
        });
    });
}

function expectToEqualFileData(items: unknown, fileNameWithExpected: string) {
    const receivedFilePath = `${paths.receivedDataDir}/${fileNameWithExpected}`;
    const itemsJson = JSON.stringify(items, undefined, 4);
    const itemsSimplified = JSON.parse(itemsJson); // E.g. Date -> string.
    writeFileText(receivedFilePath, itemsJson);

    const expectedFilePath = `${paths.expectedDataDir}/${fileNameWithExpected}`;
    const expectedItems = JSON.parse(readFileText(expectedFilePath));

    expect(itemsSimplified).toEqual(expectedItems);
}

function expectBdd(dappetizerConfig: DappetizerConfigUsingDb, entityName: string, items: Record<string, any>[]): void {
    const relFilePath = `bdd/${entityName}.md`;
    const lineBreak = '  '; // Markdown.
    const indexingConfig = dappetizerConfig.networks.mainnet!.indexing;
    const blocksRangeStr = `from ${indexingConfig.fromBlockLevel} to ${indexingConfig.toBlockLevel}`;
    const itemProperties = Object.keys(items[0]!);

    const lines = [
        `# Story: Indexing of ${startCase(entityName)}`,
        ``,
        `**As** a Token Indexer user,${lineBreak}`,
        `**When** indexing a block${lineBreak}`,
        `**I want** Token Indexer to collect respective ${startCase(entityName).toLowerCase()},${lineBreak}`,
        `**So that** I can use them further.`,
        ``,
        `## Scenario: Indexing Blocks ${blocksRangeStr}`,
        ``,
        `**When** Token Indexer indexes blocks ${blocksRangeStr}${lineBreak}`,
        `**Then** Token Indexer collects following ${startCase(entityName).toLowerCase()}:`,
        ``,
        `| ${itemProperties.map(startCase).join(' | ')} |`,
        `| ${itemProperties.map(_ => '---').join(' | ')} |`,
        ...items.map(item => `| ${itemProperties.map(p => item[p]).join(' | ')} |`),
    ];
    writeFileText(`${paths.receivedDataDir}/${relFilePath}`, lines.join(EOL));

    const expectedLines = readFileText(`${paths.expectedDataDir}/${relFilePath}`).split(/\r\n|\n|\r/u);
    expect(lines).toEqual(expectedLines);
}

function readFileText(filePath: string): string {
    return fs.readFileSync(filePath).toString();
}

function writeFileText(filePath: string, text: string): void {
    ensureDirExists(path.dirname(filePath));
    fs.writeFileSync(filePath, text);
}

function ensureDirExists(dirPath: string): void {
    if (!fs.existsSync(dirPath)) {
        fs.mkdirSync(dirPath, { recursive: true });
    }
}

function deleteFileOrDir(fileOrDirPath: string): void {
    fs.rmSync(fileOrDirPath, { recursive: true, force: true });
}
