import { registerDappetizerWithDatabase } from '@tezos-dappetizer/database';
import { INDEXER_MODULES_DI_TOKEN, ModuleCreationOptions } from '@tezos-dappetizer/indexer';
import { BackgroundWorker, ConfigElement, registerExplicitConfigFilePath } from '@tezos-dappetizer/utils';
import { container as globalContainer, DependencyContainer } from 'tsyringe';

import { TokenIndexerConfigValidator } from '../src/config/token-indexer-config-validator';
import { TOKEN_INDEXER_MODULE_ID } from '../src/config/token-indexer-internal-config';
import { TokenMetadataBackgroundWorker } from '../src/metadata/token/token-metadata-background-worker';
import { indexerModule } from '../src/public-api';

describe('dependency injection', () => {
    let diContainer: DependencyContainer;

    beforeAll(() => {
        diContainer = globalContainer.createChildContainer();
        registerDappetizerWithDatabase(diContainer);
        registerExplicitConfigFilePath(diContainer, { useValue: '../../dappetizer.config.ts' });

        const tokenIndexerModule = indexerModule({
            diContainer,
            configElement: new ConfigElement(undefined, 'dappetizer.config.ts', '$.tokenIndexer'),
        } as ModuleCreationOptions);
        diContainer.registerInstance(INDEXER_MODULES_DI_TOKEN, tokenIndexerModule);
    });

    it(`should resolve ${INDEXER_MODULES_DI_TOKEN as string}`, () => {
        const modules = diContainer.resolveAll(INDEXER_MODULES_DI_TOKEN);

        expect(modules.map(m => m.name)).toEqual([TOKEN_INDEXER_MODULE_ID]);
    });

    it('should resolve background workers from token indexer', () => {
        const workers = diContainer.resolveAll<BackgroundWorker>('Dappetizer:Utils:BackgroundWorkers');

        expect(workers.map(w => w.constructor)).toIncludeAllMembers([TokenMetadataBackgroundWorker, TokenIndexerConfigValidator]);
    });
});
