import { Gauge } from 'prom-client';
import { anything, instance, mock, verify } from 'ts-mockito';

import { TokenIndexerInternalConfig } from '../../../src/config/token-indexer-internal-config';
import { DbTokenKey } from '../../../src/entities/db-token';
import { TokenIndexerMetrics } from '../../../src/helpers/token-indexer-metrics';
import { TokenMetadataBackgroundDispatcher } from '../../../src/metadata/token/token-metadata-background-dispatcher';
import { TokenMetadataBackgroundUpdater } from '../../../src/metadata/token/token-metadata-background-updater';

describe(TokenMetadataBackgroundDispatcher.name, () => {
    let target: TokenMetadataBackgroundDispatcher;
    let updater: TokenMetadataBackgroundUpdater;
    let metrics: TokenIndexerMetrics;

    beforeEach(() => {
        updater = mock<TokenMetadataBackgroundUpdater>();
        metrics = { tokenMetadataUpdating: mock(Gauge) } as TokenIndexerMetrics;
        const metricsInstance = { tokenMetadataUpdating: instance(metrics.tokenMetadataUpdating) } as TokenIndexerMetrics;
        const config = { metadataUpdateParallelism: 2 } as TokenIndexerInternalConfig;
        target = new TokenMetadataBackgroundDispatcher(instance(updater), metricsInstance, config);
    });

    it('should dispatch an update', () => {
        const tokenKey = {} as DbTokenKey;

        target.updateOnBackground(tokenKey);

        expect(target.remainingCount).toBe(1);
        verify(updater.update(tokenKey)).once();
        verify(metrics.tokenMetadataUpdating.inc()).once();
        verify(metrics.tokenMetadataUpdating.dec()).never();
    });

    it('should finish an update asynchronously', async () => {
        const tokenKey = {} as DbTokenKey;
        target.updateOnBackground(tokenKey);

        await target.completeAllRemaining();

        expect(target.remainingCount).toBe(0);
        verify(metrics.tokenMetadataUpdating.dec()).once();
    });

    it('should dispatch only configured number of updates', () => {
        const tokenKey1 = {} as DbTokenKey;
        const tokenKey2 = {} as DbTokenKey;
        const tokenKey3 = {} as DbTokenKey;

        target.updateOnBackground(tokenKey1);
        target.updateOnBackground(tokenKey2);
        target.updateOnBackground(tokenKey3);

        expect(target.remainingCount).toBe(3);
        verify(updater.update(anything())).times(2);
        verify(updater.update(tokenKey1)).once();
        verify(updater.update(tokenKey2)).once();
        verify(metrics.tokenMetadataUpdating.inc()).times(3);
    });

    it('should dispatch waiting updates once former ones finish', async () => {
        const tokenKey1 = {} as DbTokenKey;
        const tokenKey2 = {} as DbTokenKey;
        const tokenKey3 = {} as DbTokenKey;

        target.updateOnBackground(tokenKey1);
        target.updateOnBackground(tokenKey2);
        target.updateOnBackground(tokenKey3);

        await target.completeAllRemaining();

        expect(target.remainingCount).toBe(0);
        verify(updater.update(anything())).times(3);
        verify(updater.update(tokenKey1)).once();
        verify(updater.update(tokenKey2)).once();
        verify(updater.update(tokenKey3)).once();
    });

    it('should complete asap if no updates remaining', async () => {
        await target.completeAllRemaining();

        expect(target.remainingCount).toBe(0);
        verify(updater.update(anything())).never();
    });
});
