import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import BigNumber from 'bignumber.js';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { MetadataState } from '../../../src/entities/metadata-state';
import { contractAddresses } from '../../../src/helpers/contract-addresses';
import { HardcodedTokenMetadataProvider } from '../../../src/metadata/token/hardcoded-token-metadata-provider';
import { TokenMetadata } from '../../../src/metadata/token/token-metadata';
import { TokenMetadataProvider } from '../../../src/metadata/token/token-metadata-provider';

describe(HardcodedTokenMetadataProvider.name, () => {
    let target: HardcodedTokenMetadataProvider;
    let nextProvider: TokenMetadataProvider;

    beforeEach(() => {
        nextProvider = mock();
        target = new HardcodedTokenMetadataProvider(instance(nextProvider));
    });

    it('should get hardcoded metadata if contract and token ID matched', async () => {
        const contract = { address: contractAddresses.USD_TZ } as ContractAbstraction;

        const metadata = await target.get(contract, new BigNumber(0));

        expect(metadata).toEqual<TokenMetadata>({
            metadataState: MetadataState.Ok,
            decimals: 6,
            metadata: '{"symbol":"USDtz","name":"USDtez","decimals":6}',
            name: 'USDtez',
            symbol: 'USDtz',
        });
        verify(nextProvider.get(anything(), anything())).never();
    });

    it.each([
        ['contract is not matched', 'mockedAddr', new BigNumber(0)],
        ['token ID is not matched', contractAddresses.USD_TZ, new BigNumber(1)],
    ])('should delegate to next provider if no hardcoded metadata b/c %s', async (_desc, address, tokenId) => {
        const contract = { address } as ContractAbstraction;
        when(nextProvider.get(contract, tokenId)).thenResolve('mockedMetadata' as any);

        const metadata = await target.get(contract, tokenId);

        expect(metadata).toBe('mockedMetadata');
    });
});
