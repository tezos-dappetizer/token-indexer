import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import { instance, mock, when } from 'ts-mockito';

import { MetadataState } from '../../../src/entities/metadata-state';
import { contractAddresses } from '../../../src/helpers/contract-addresses';
import { ContractMetadata } from '../../../src/metadata/contract/contract-metadata';
import { ContractMetadataProvider } from '../../../src/metadata/contract/contract-metadata-provider';
import { HardcodedContractMetadataProvider } from '../../../src/metadata/contract/hardcoded-contract-metadata-provider';

describe(HardcodedContractMetadataProvider.name, () => {
    let target: HardcodedContractMetadataProvider;
    let nextProvider: ContractMetadataProvider;

    beforeEach(() => {
        nextProvider = mock();
        target = new HardcodedContractMetadataProvider(instance(nextProvider));
    });

    it('should get hardcoded metadata if contract and token ID matched', async () => {
        const contract = { address: contractAddresses.USD_TZ } as ContractAbstraction;

        const metadata = await target.get(contract);

        expect(metadata).toEqual<ContractMetadata>({
            metadataState: MetadataState.Ok,
            name: 'USDtz',
            description: null,
        });
    });

    it('should delegate to next provider if no hardcoded metadata matched', async () => {
        const contract = { address: 'mockedAddr' } as ContractAbstraction;
        when(nextProvider.get(contract)).thenResolve('mockedMetadata' as any);

        const metadata = await target.get(contract);

        expect(metadata).toBe('mockedMetadata');
    });
});
