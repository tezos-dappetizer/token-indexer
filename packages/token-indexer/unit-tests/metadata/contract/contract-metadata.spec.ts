import { MetadataState } from '../../../src/entities/metadata-state';
import { emptyContractMetadata, ContractMetadata } from '../../../src/metadata/contract/contract-metadata';

describe('ContractMetadata', () => {
    it('should provide correct empty metadata', () => {
        expect(emptyContractMetadata).toEqual<Partial<Record<MetadataState, ContractMetadata>>>({
            Failed: {
                metadataState: MetadataState.Failed,
                description: null,
                name: null,
            },
            Ok: {
                metadataState: MetadataState.Ok,
                description: null,
                name: null,
            },
        });
    });
});
