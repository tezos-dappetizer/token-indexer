import { ConfigElement } from '@tezos-dappetizer/utils';
import { StrictOmit } from 'ts-essentials';

import { TokenIndexerConfig } from '../../src/config/token-indexer-config';
import { TokenIndexerInternalConfig } from '../../src/config/token-indexer-internal-config';

type TokenIndexerInternalConfigProperties = StrictOmit<TokenIndexerInternalConfig, 'getDiagnostics'>;

describe(TokenIndexerInternalConfig.name, () => {
    function act(thisJson: TokenIndexerConfig | undefined) {
        return new TokenIndexerInternalConfig(new ConfigElement(thisJson, 'C:/config.json', '$.tokenIndexer'));
    }

    it('should be created with all values specified', () => {
        const thisJson: TokenIndexerConfig = {
            logIncompatibleContracts: true,
            logIncompatibleContractsThreshold: 22,
            skipGetMetadata: true,
            metadataUpdateParallelism: 202,
            contractNames: ['Foo'],
            contractMetadataCacheMillis: 333,
        };

        const config = act(thisJson);

        expect(config).toEqual<TokenIndexerInternalConfigProperties>({
            configFilePath: 'C:/config.json',
            configPropertyPath: '$.tokenIndexer',

            logIncompatibleContracts: true,
            logIncompatibleContractsThreshold: 22,
            skipGetMetadata: true,
            metadataUpdateParallelism: 202,
            contractNames: ['Foo'],
            contractMetadataCacheMillis: 333,
        });
    });

    it.each([
        ['undefined', undefined],
        ['empty', {}],
    ])('should be created with defaults if %s config element', (_desc, inputValue) => {
        const config = act(inputValue);

        expect(config).toEqual<TokenIndexerInternalConfigProperties>({
            configFilePath: 'C:/config.json',
            configPropertyPath: '$.tokenIndexer',

            logIncompatibleContracts: false,
            logIncompatibleContractsThreshold: 10,
            skipGetMetadata: false,
            metadataUpdateParallelism: 100,
            contractNames: null,
            contractMetadataCacheMillis: 600_000,
        });
    });
});
