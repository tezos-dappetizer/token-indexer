import { Contract, ContractProvider, SelectiveIndexingConfig } from '@tezos-dappetizer/indexer';
import { ConfigError } from '@tezos-dappetizer/utils';
import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrowAsync, TestLogger } from '../../../../test-utilities/mocks';
import { TokenIndexerConfigValidator } from '../../src/config/token-indexer-config-validator';
import { TokenIndexerInternalConfig } from '../../src/config/token-indexer-internal-config';
import { CompositeContractTypeResolver } from '../../src/indexers/composite-contract-type-resolver';
import { ContractType } from '../../src/indexers/contract-type-resolver';

describe(TokenIndexerConfigValidator.name, () => {
    let target: TokenIndexerConfigValidator;
    let tokenIndexerConfig: Writable<TokenIndexerInternalConfig>;
    let indexingConfig: Writable<SelectiveIndexingConfig>;
    let contractProvider: ContractProvider;
    let contractTypeResolver: CompositeContractTypeResolver;
    let logger: TestLogger;

    let fooContract: Contract;
    let barContract1: Contract;
    let barContract2: Contract;

    beforeEach(() => {
        target = new TokenIndexerConfigValidator(
            tokenIndexerConfig = {} as typeof tokenIndexerConfig,
            indexingConfig = {} as typeof indexingConfig,
            instance(contractProvider = mock()),
            instance(contractTypeResolver = mock()),
            logger = new TestLogger(),
        );

        fooContract = 'mockedFooContract' as any;
        barContract1 = 'mockedBarContract1' as any;
        barContract2 = 'mockedBarContract2' as any;

        tokenIndexerConfig.contractNames = ['Foo', 'Bar'];
        tokenIndexerConfig.configFilePath = 'C:/config.json';
        tokenIndexerConfig.configPropertyPath = '$.tokenIndexer';
        indexingConfig.contracts = [
            { name: 'Foo', addresses: ['fooAddr'] },
            { name: 'Bar', addresses: ['barAddr1', 'barAddr2'] },
        ];
        indexingConfig.configPropertyPath = '$.indexing';
        when(contractProvider.getContract('fooAddr')).thenResolve(fooContract);
        when(contractProvider.getContract('barAddr1')).thenResolve(barContract1);
        when(contractProvider.getContract('barAddr2')).thenResolve(barContract2);
        when(contractTypeResolver.resolveContractType(fooContract)).thenReturn(ContractType.FA1_2_like);
        when(contractTypeResolver.resolveContractType(barContract2)).thenReturn(ContractType.FA2_like);
    });

    it('should pass if FA contracts', async () => {
        await target.start();

        verify(contractTypeResolver.resolveContractType(anything())).times(3);
        logger.verifyNothingLogged();
    });

    it('should pass if no "contractNames" configured', async () => {
        tokenIndexerConfig.contractNames = null;

        await target.start();

        verify(contractProvider.getContract(anything())).never();
        logger.verifyNothingLogged();
    });

    it('should throw if no "indexing.contracts" configured', async () => {
        indexingConfig.contracts = null;

        await runThrowTest([
            `selective indexing is disabled`,
            `in '$.indexing.contracts'`,
            `'contractNames' cannot`,
        ]);

        verify(contractProvider.getContract(anything())).never();
    });

    it('should throw if contract name is not configured in indexing config', async () => {
        indexingConfig.contracts = [{ name: 'Foo', addresses: ['fooAddr'] }];

        await runThrowTest([
            `Name 'Bar'`,
            `missing in '$.indexing.contracts'`,
            `containing ['Foo']`,
        ]);
    });

    it('should throw if contract does not comply to FA', async () => {
        when(contractTypeResolver.resolveContractType(fooContract)).thenReturn(null);

        await runThrowTest([
            `Name 'Foo'`,
            `addresses ['fooAddr'] from '$.indexing.contracts'`,
            `do not comply to FA1.2 nor FA2`,
            `by @tezos-dappetizer/token-indexer`,
        ]);
    });

    it('should log if not config error', async () => {
        const error = new Error('oups');
        when(contractProvider.getContract(anything())).thenReject(error);

        await target.start();

        logger.loggedSingle().verify('Error', {
            configPropertyPath: '$.tokenIndexer.contractNames',
            configFile: 'C:/config.json',
            error,
        });
    });

    async function runThrowTest(expectedReason: string[]) {
        const error = await expectToThrowAsync(async () => target.start(), ConfigError);

        expect(error.filePath).toBe('C:/config.json');
        expect(error.propertyPath).toBe('$.tokenIndexer.contractNames');
        expect(error.value).toEqual(['Foo', 'Bar']);
        expectedReason.forEach(s => expect(error.reason).toContain(s));
        logger.verifyNothingLogged();
    }
});
