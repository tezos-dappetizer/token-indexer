import { Michelson } from '@tezos-dappetizer/indexer';

import { removeAnnots } from '../../src/helpers/michelson-utils';

describe(removeAnnots.name, () => {
    it('should remove annotations deeply', () => {
        const input: Michelson = {
            prim: 'big_map',
            args: [
                { prim: 'address', annots: [':user'] },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'nat', annots: [':balance'], args: [] }, // Empty args
                        {
                            prim: 'map',
                            args: [
                                { prim: 'address' }, // No annots
                                { prim: 'nat', annots: [] }, // Empty annots
                            ],
                            annots: [':approvals'],
                        },
                    ],
                },
            ],
            annots: ['%ledger'],
        };

        const result = removeAnnots(input);

        expect(result).toEqual<Michelson>({
            prim: 'big_map',
            args: [
                { prim: 'address' },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'nat' },
                        {
                            prim: 'map',
                            args: [
                                { prim: 'address' },
                                { prim: 'nat' },
                            ],
                        },
                    ],
                },
            ],
        });
    });
});
