import '../../../../test-utilities/mocks/set-sqlite-db-types';

import { BigNumber } from 'bignumber.js';

import { TestLogger } from '../../../../test-utilities/mocks';
import { TokenIndexerDbInitializer } from '../../src/helpers/token-indexer-db-initializer';
import { getCmdTestHelper, TestDatabase } from '../test-database';

describe('Current balance view', () => {
    let db: TestDatabase;

    beforeEach(async () => {
        db = await getCmdTestHelper();
    });

    afterEach(async () => {
        await db.destroy();
    });

    it('should return correct current balances', async () => {
        const initializer = new TokenIndexerDbInitializer(new TestLogger());
        await initializer.afterSynchronization(db.dataSource);

        const nextBlockHash = 'BL6bvFJuCBhs4M6AC3cdgQoYhSPzuWCGVfHW8zFPMqgYtiAt2UR';
        const contractAddr = 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9';
        const tokenId1 = new BigNumber(111);
        const tokenId2 = new BigNumber(222);
        const ownerAddr = 'tz1efdBSPksB8RykXwUFG9rb5qLPbfYcwrtN';

        await db.insertBlock({ hash: nextBlockHash });
        await db.insertContract({ addr: contractAddr });

        await db.insertToken({ contractAddr, id: tokenId1 });
        await db.insertBalance({ contractAddr, tokenId: tokenId1, ownerAddr, amount: 1, validFromBlockHash: db.firstBlockHash });

        await db.insertToken({ contractAddr, id: tokenId2 });
        await db.insertBalance({
            contractAddr,
            tokenId: tokenId2,
            ownerAddr,
            amount: 2,
            validFromBlockHash: db.firstBlockHash,
            validUntilBlockHash: nextBlockHash,
        });
        await db.insertBalance({ contractAddr, tokenId: tokenId2, ownerAddr, amount: 300, validFromBlockHash: nextBlockHash });

        await db.verifyData(`SELECT owner_address, amount, operation_group_hash, token_id, token_contract_address, valid_from_block_hash 
            FROM current_balance 
            WHERE owner_address = '${ownerAddr}'`, [{
            owner_address: ownerAddr,
            amount: 1,
            operation_group_hash: db.firstOperationGroupHash,
            token_id: tokenId1.toNumber(),
            token_contract_address: contractAddr,
            valid_from_block_hash: db.firstBlockHash,
        }, {
            owner_address: ownerAddr,
            amount: 300,
            operation_group_hash: db.firstOperationGroupHash,
            token_id: tokenId2.toNumber(),
            token_contract_address: contractAddr,
            valid_from_block_hash: nextBlockHash,
        }]);
    });
});
