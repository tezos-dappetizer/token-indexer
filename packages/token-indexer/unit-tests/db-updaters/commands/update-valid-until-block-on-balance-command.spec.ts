import '../../../../../test-utilities/mocks/set-sqlite-db-types';

import { BigNumber } from 'bignumber.js';

import { describeMember } from '../../../../../test-utilities/mocks';
import {
    UpdateValidUntilBlockOnBalanceCommand,
} from '../../../src/db-updaters/commands/update-valid-until-block-on-balance-command';
import { getCmdTestHelper, TestDatabase } from '../../test-database';

describe(UpdateValidUntilBlockOnBalanceCommand.name, () => {
    describeMember<UpdateValidUntilBlockOnBalanceCommand>('description', () => {
        it('should contain class name and arguments', () => {
            const filter = {
                contractAddress: 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9',
                tokenId: new BigNumber(123),
                ownerAddress: 'tz1efdBSPksB8RykXwUFG9rb5qLPbfYcwrtN',
            };
            const validUntilBlockHashToSet = 'BKyrNsvS6Q8KDmmqpQWWuakSwh2HC9CachoYjMtM1W6QoWfvAcv';

            const target = new UpdateValidUntilBlockOnBalanceCommand({ filter, validUntilBlockHashToSet });

            expect(target.description).toIncludeMultiple([
                UpdateValidUntilBlockOnBalanceCommand.name,
                `'${filter.contractAddress}'`,
                `'${validUntilBlockHashToSet}'`,
            ]);
        });
    });

    describeMember<UpdateValidUntilBlockOnBalanceCommand>('execute', () => {
        let db: TestDatabase;

        const validUntilBlockHashToSet = 'BKyrNsvS6Q8KDmmqpQWWuakSwh2HC9CachoYjMtM1W6QoWfvAcv';
        const otherValidUntilBlockHash = 'BMAEsJzwSS2aWcezj4GS5CBwrumc1FTrDzBfYa9AcTENqndsiyr';
        const contractAddr = 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9';
        const unrelatedContractAddr = 'KT1QxLqukyfohPV5kPkw97Rs6cw1DDDvYgbB';
        const tokenId = new BigNumber(111);
        const unrelatedTokenId = new BigNumber(222);
        const ownerAddr1 = 'tz1efdBSPksB8RykXwUFG9rb5qLPbfYcwrtN';
        const ownerAddr2 = 'tz1ShM5gWyLSD997WV89nzZWxPXFGBMJ2yRw';

        beforeEach(async () => {
            db = await getCmdTestHelper();

            await db.insertBlock({ hash: validUntilBlockHashToSet });
            await db.insertBlock({ hash: otherValidUntilBlockHash });

            await db.insertContract({ addr: contractAddr });
            await db.insertContract({ addr: unrelatedContractAddr });

            await db.insertToken({ contractAddr, id: tokenId });
            await db.insertToken({ contractAddr, id: unrelatedTokenId });
            await db.insertToken({ contractAddr: unrelatedContractAddr, id: tokenId });

            await db.insertBalance({ contractAddr, tokenId, ownerAddr: ownerAddr1, amount: 11, validFromBlockHash: otherValidUntilBlockHash });
            await db.insertBalance({ contractAddr, tokenId, ownerAddr: ownerAddr2, amount: 22 });
            await db.insertBalance({ contractAddr, tokenId, ownerAddr: ownerAddr1, amount: 33, validUntilBlockHash: otherValidUntilBlockHash });
            await db.insertBalance({ contractAddr, tokenId: unrelatedTokenId, ownerAddr: ownerAddr1, amount: 44 });
            await db.insertBalance({ contractAddr: unrelatedContractAddr, tokenId, ownerAddr: ownerAddr1, amount: 55 });
        });

        afterEach(async () => {
            await db.destroy();
        });

        it('should set next block for particular owner of the token', async () => {
            await db.executeCmd(new UpdateValidUntilBlockOnBalanceCommand({
                filter: {
                    contractAddress: contractAddr,
                    tokenId: new BigNumber(tokenId),
                    ownerAddress: ownerAddr1,
                },
                validUntilBlockHashToSet,
            }));

            await verifyBalancesInDb([
                { amount: 11, valid_until_block_hash: validUntilBlockHashToSet },
                { amount: 22, valid_until_block_hash: null },
                { amount: 33, valid_until_block_hash: otherValidUntilBlockHash },
                { amount: 44, valid_until_block_hash: null },
                { amount: 55, valid_until_block_hash: null },
            ]);
        });

        it('should set next block for all owners of the token', async () => {
            await db.executeCmd(new UpdateValidUntilBlockOnBalanceCommand({
                filter: {
                    contractAddress: contractAddr,
                    tokenId: new BigNumber(tokenId),
                },
                validUntilBlockHashToSet,
            }));

            await verifyBalancesInDb([
                { amount: 11, valid_until_block_hash: validUntilBlockHashToSet },
                { amount: 22, valid_until_block_hash: validUntilBlockHashToSet },
                { amount: 33, valid_until_block_hash: otherValidUntilBlockHash },
                { amount: 44, valid_until_block_hash: null },
                { amount: 55, valid_until_block_hash: null },
            ]);
        });

        async function verifyBalancesInDb(expected: { amount: number; valid_until_block_hash: string | null }[]) {
            await db.verifyData('SELECT amount, valid_until_block_hash FROM balance', expected);
        }
    });
});
