import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { NFT_DEFAULT_AMOUNT } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { ListMintProcessor } from '../../src/indexers/entrypoints/mint/processors/list-mint-processor';
import { Tzip12TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip12-transfer-processor';

export const SergioPerezSpecialEdition: TestContract = {
    address: 'KT1NaiY8YoUMwhCzGa2NsSZPG5MRWKkyTyge',
    name: 'Sergio Perez Special Edition Race Suit - Mexican GP, 2021',
    bigMapName: 'ledger',
    contractType: ContractType.FA2_like,
    mint: {
        expectedProcessor: ListMintProcessor,
        schema: {
            prim: 'list',
            args: [{
                prim: 'pair',
                args: [
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'nat', annots: ['%token_id'] },
                            {
                                prim: 'map',
                                args: [
                                    { prim: 'string' },
                                    { prim: 'bytes' },
                                ],
                                annots: ['%token_info'],
                            },
                        ],
                        annots: ['%token_metadata'],
                    },
                    { prim: 'address', annots: ['%owner'] },
                ],
            }],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    {
                        prim: 'Pair',
                        args: [
                            { int: '5' },
                            [
                                {
                                    prim: 'Elt',
                                    args: [
                                        { string: '' },
                                        // eslint-disable-next-line max-len
                                        { bytes: '697066733a2f2f516d59343233717855774d36484c3273564862685550326b624a77723974353762634146734d745879774165744d' },
                                    ],
                                },
                                {
                                    prim: 'Elt',
                                    args: [
                                        { string: 'name' },
                                        // eslint-disable-next-line max-len
                                        { bytes: '53657267696f20506572657a205370656369616c2045646974696f6e20526163652053756974202d204d65786963616e2047502c2032303231204e6f2e2031' },
                                    ],
                                },
                            ],
                        ],
                    },
                    { string: 'tz1d3fYBynJEVXrWTmR4p5hggtwcbg5fdsjo' },
                ],
            }],
            expected: [{
                tokenId: new BigNumber('5'),
                toAddress: 'tz1d3fYBynJEVXrWTmR4p5hggtwcbg5fdsjo',
                amount: NFT_DEFAULT_AMOUNT,
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip12TransferProcessor,
        schema: {
            prim: 'list',
            args: [{
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%from_'] },
                    {
                        prim: 'list',
                        args: [{
                            prim: 'pair',
                            args: [
                                { prim: 'address', annots: ['%to_'] },
                                {
                                    prim: 'pair',
                                    args: [
                                        { prim: 'nat', annots: ['%token_id'] },
                                        { prim: 'nat', annots: ['%amount'] },
                                    ],
                                },
                            ],
                        }],
                        annots: ['%txs'],
                    },
                ],
            }],
            annots: ['%transfer'],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { bytes: '0001fae277e1c1e0ec097b994c4f291d0a5a524757ff' },
                    [{
                        prim: 'Pair',
                        args: [
                            { bytes: '0102875b6cdb1e87251a32b8be1ec65fce4b56c77700' },
                            {
                                prim: 'Pair',
                                args: [
                                    { int: '41945' },
                                    { int: '8' },
                                ],
                            },
                        ],
                    }],
                ],
            }],
            expected: [{
                fromAddress: 'tz2XBntayJQK5sDaSWsYgtGC2YUmZev1R73F',
                toAddress: 'KT18p94vjkkHYY3nPmernmgVR7HdZFzE7NAk',
                tokenId: new BigNumber(41945),
                amount: new BigNumber(8),
            }],
        },
    },
};
