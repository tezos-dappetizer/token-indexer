import BigNumber from 'bignumber.js';

import { TestContract } from './test-contract';
import { contractAddresses } from '../../src/helpers/contract-addresses';
import { NFT_DEFAULT_AMOUNT } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import {
    TezosMandalaMintProcessor,
} from '../../src/indexers/entrypoints/mint/processors/incompatible/tezos-mandala-mint-processor';
import { Tzip12TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip12-transfer-processor';

export const TezosMandalaV2: TestContract = {
    address: contractAddresses.TEZOS_MANDALA,
    name: 'Tezos Mandala',
    bigMapName: 'ledger',
    contractType: ContractType.FA2_like,
    mint: {
        expectedProcessor: TezosMandalaMintProcessor,
        schema: { prim: 'nat' },
        sample: {
            input: { int: '171' },
            expected: [{
                tokenId: new BigNumber('171'),
                toAddress: 'operation result parent source',
                amount: NFT_DEFAULT_AMOUNT,
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip12TransferProcessor,
        schema: {
            prim: 'list',
            args: [
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%from_'] },
                        {
                            prim: 'list',
                            args: [
                                {
                                    prim: 'pair',
                                    args: [
                                        { prim: 'address', annots: ['%to_'] },
                                        {
                                            prim: 'pair',
                                            args: [
                                                { prim: 'nat', annots: ['%token_id'] },
                                                { prim: 'nat', annots: ['%amount'] },
                                            ],
                                        },
                                    ],
                                },
                            ],
                            annots: ['%txs'],
                        },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { string: 'tz1eeq85Mq5Ypi19RsCWgc3QtzHKE4NnkjKR' },
                    [{
                        prim: 'Pair',
                        args: [
                            { string: 'tz1h6mySdKmhv23eJXdWcFxJREr8qw2t7Nxu' },
                            {
                                prim: 'Pair',
                                args: [
                                    { int: '697' },
                                    { int: '1' },
                                ],
                            },
                        ],
                    }],
                ],
            }],
            expected: [{
                fromAddress: 'tz1eeq85Mq5Ypi19RsCWgc3QtzHKE4NnkjKR',
                toAddress: 'tz1h6mySdKmhv23eJXdWcFxJREr8qw2t7Nxu',
                tokenId: new BigNumber(697),
                amount: new BigNumber(1),
            }],
        },
    },
};
