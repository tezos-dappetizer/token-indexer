import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { ListBurnProcessor } from '../../src/indexers/entrypoints/burn/processors/list-burn-processor';
import { ListMintProcessor } from '../../src/indexers/entrypoints/mint/processors/list-mint-processor';
import { Tzip12TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip12-transfer-processor';

export const SebuhNet: TestContract = {
    address: 'KT1981tPmXh4KrUQKZpQKb55kREX7QGJcF3E',
    name: 'Sebuh.net',
    bigMapName: 'ledger',
    contractType: ContractType.FA2_like,
    mint: {
        expectedProcessor: ListMintProcessor,
        schema: {
            prim: 'list',
            args: [{
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%owner'] },
                    { prim: 'nat', annots: ['%amount'] },
                ],
            }],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { string: 'tz1dXpsNAWLHVYNWSqhrHCHT6wh6L1fKZfo1' },
                    { int: '100' },
                ],
            }],
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                toAddress: 'tz1dXpsNAWLHVYNWSqhrHCHT6wh6L1fKZfo1',
                amount: new BigNumber('100'),
            }],
        },
    },
    burn: {
        expectedProcessor: ListBurnProcessor,
        schema: {
            prim: 'list',
            args: [
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%owner'] },
                        { prim: 'nat', annots: ['%amount'] },
                    ],
                },
            ],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { string: 'tz1b58mxvQAc98hbXjcrgjf5xnSwYXzi5gX6' },
                    { int: '10800' },
                ],
            }],
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: 'tz1b58mxvQAc98hbXjcrgjf5xnSwYXzi5gX6',
                amount: new BigNumber('10800'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip12TransferProcessor,
        schema: {
            prim: 'list',
            args: [
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%from_'] },
                        {
                            prim: 'list',
                            args: [
                                {
                                    prim: 'pair',
                                    args: [
                                        { prim: 'address', annots: ['%to_'] },
                                        {
                                            prim: 'pair',
                                            args: [
                                                { prim: 'nat', annots: ['%token_id'] },
                                                { prim: 'nat', annots: ['%amount'] },
                                            ],
                                        },
                                    ],
                                },
                            ],
                            annots: ['%txs'],
                        },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { string: 'tz1NufWtpqS3nfR8VW1xFyWq4GWqb969keeR' },
                    [{
                        prim: 'Pair',
                        args: [
                            { string: 'tz1iocTPmx2ZzPiqLBfz8hsu9fTGdLz4NfXA' },
                            {
                                prim: 'Pair',
                                args: [
                                    { int: '0' },
                                    { int: '1' },
                                ],
                            },
                        ],
                    }],
                ],
            }],
            expected: [{
                fromAddress: 'tz1NufWtpqS3nfR8VW1xFyWq4GWqb969keeR',
                toAddress: 'tz1iocTPmx2ZzPiqLBfz8hsu9fTGdLz4NfXA',
                tokenId: SINGLE_ASSET_TOKEN_ID,
                amount: new BigNumber(1),
            }],
        },
    },
};
