import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { BurnProcessor } from '../../src/indexers/entrypoints/burn/processors/burn-processor';
import { MintProcessor } from '../../src/indexers/entrypoints/mint/processors/mint-processor';
import { Tzip12TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip12-transfer-processor';

export const YouvesSyntheticAsset: TestContract = {
    address: 'KT1XRPEPXbZK25r3Htzp2o1x7xdMMmfocKNW',
    name: 'Youves Synthetic Asset',
    bigMapName: 'ledger',
    contractType: ContractType.FA2_like,
    mint: {
        expectedProcessor: MintProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%owner'] },
                { prim: 'nat', annots: ['%token_id'] },
                { prim: 'nat', annots: ['%token_amount'] },
            ],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { string: 'tz1VwoDxLBarwvM7KZXJRem1DZmefdYKpstk' },
                    { int: '98437500000' },
                    { int: '0' },
                ],
            },
            expected: [{
                tokenId: new BigNumber('98437500000'),
                toAddress: 'tz1VwoDxLBarwvM7KZXJRem1DZmefdYKpstk',
                amount: new BigNumber('0'),
            }],
        },
    },
    burn: {
        expectedProcessor: BurnProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%owner'] },
                { prim: 'nat', annots: ['%token_id'] },
                { prim: 'nat', annots: ['%token_amount'] },
            ],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { string: 'KT1M8asPmVQhFG6yujzttGonznkghocEkbFk' },
                    { int: '200000000000' },
                    { int: '0' },
                ],
            },
            expected: [{
                tokenId: new BigNumber('200000000000'),
                fromAddress: 'KT1M8asPmVQhFG6yujzttGonznkghocEkbFk',
                amount: new BigNumber('0'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip12TransferProcessor,
        schema: {
            prim: 'list',
            args: [{
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%from_'] },
                    {
                        prim: 'list',
                        args: [
                            {
                                prim: 'pair',
                                args: [
                                    { prim: 'address', annots: ['%to_'] },
                                    {
                                        prim: 'pair',
                                        args: [
                                            { prim: 'nat', annots: ['%token_id'] },
                                            { prim: 'nat', annots: ['%amount'] },
                                        ],
                                    },
                                ],
                            },
                        ],
                        annots: ['%txs'],
                    },
                ],
            }],
            annots: ['%transfer'],
        },
        sample: {
            input: [
                {
                    prim: 'Pair',
                    args: [
                        { bytes: '000068ae51b050282c1135897a3b902474324103d220' },
                        [{
                            prim: 'Pair',
                            args: [
                                { bytes: '0168af4dc6d5632968e3b872d5d1f92c2d73c5c96c00' },
                                {
                                    prim: 'Pair',
                                    args: [
                                        { int: '0' },
                                        { int: '70669333336' },
                                    ],
                                },
                            ],
                        }],
                    ],
                },
            ],
            expected: [{
                fromAddress: 'tz1VBXnyNVVZWpUDuSG19Kr5Wrb8pkdYQKGj',
                toAddress: 'KT1J8Hr3BP8bpbfmgGpRPoC9nAMSYtStZG43',
                tokenId: new BigNumber(0),
                amount: new BigNumber(70669333336),
            }],
        },
    },
};
