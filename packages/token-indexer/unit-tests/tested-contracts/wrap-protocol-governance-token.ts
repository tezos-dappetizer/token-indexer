import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { ListBurnProcessor } from '../../src/indexers/entrypoints/burn/processors/list-burn-processor';
import { ListMintProcessor } from '../../src/indexers/entrypoints/mint/processors/list-mint-processor';
import { Tzip12TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip12-transfer-processor';

export const WrapProtocolGovernanceToken: TestContract = {
    address: 'KT1LRboPna9yQY9BrjtQYDS1DVxhKESK4VVd',
    name: 'Wrap protocol governance token',
    bigMapName: 'ledger',
    contractType: ContractType.FA2_like,
    mint: {
        expectedProcessor: ListMintProcessor,
        schema: {
            prim: 'list',
            args: [{
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%owner'] },
                    { prim: 'nat', annots: ['%token_id'] },
                    { prim: 'nat', annots: ['%amount'] },
                ],
            }],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { string: 'KT1MTnKjFSN2u4myPRBqnFuCYCPX8kTdUEnv' },
                    { int: '0' },
                    { int: '149775' },
                ],
            }],
            expected: [{
                tokenId: new BigNumber('0'),
                toAddress: 'KT1MTnKjFSN2u4myPRBqnFuCYCPX8kTdUEnv',
                amount: new BigNumber('149775'),
            }],
        },
    },
    burn: {
        expectedProcessor: ListBurnProcessor,
        schema: {
            prim: 'list',
            args: [{
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%owner'] },
                    { prim: 'nat', annots: ['%token_id'] },
                    { prim: 'nat', annots: ['%amount'] },
                ],
            }],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { string: 'tz1NRbpmTuPEFCCYCqCr7i3hkMRDRhBFgmxz' },
                    { int: '0' },
                    { int: '100000000' },
                ],
            }],
            expected: [{
                tokenId: new BigNumber('0'),
                fromAddress: 'tz1NRbpmTuPEFCCYCqCr7i3hkMRDRhBFgmxz',
                amount: new BigNumber('100000000'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip12TransferProcessor,
        schema: {
            prim: 'list',
            args: [
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%from_'] },
                        {
                            prim: 'list',
                            args: [{
                                prim: 'pair',
                                args: [
                                    { prim: 'address', annots: ['%to_'] },
                                    {
                                        prim: 'pair',
                                        args: [
                                            { prim: 'nat', annots: ['%token_id'] },
                                            { prim: 'nat', annots: ['%amount'] },
                                        ],
                                    },
                                ],
                            }],
                            annots: ['%txs'],
                        },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { bytes: '00002a86d0d3e0bad8004a5dba65cd46d73e892c3eac' },
                    [{
                        prim: 'Pair',
                        args: [
                            { bytes: '01494067a9e4ba27e2d26c51d1659c7ebaabe68d0f00' },
                            {
                                prim: 'Pair',
                                args: [
                                    { int: '0' },
                                    { int: '19530979944' },
                                ],
                            },
                        ],
                    }],
                ],
            }],
            expected: [{
                fromAddress: 'tz1PWtaLXKiHXhXGvpuS8w4sVveNRKedTRSe',
                toAddress: 'KT1FG63hhFtMEEEtmBSX2vuFmP87t9E7Ab4t',
                tokenId: SINGLE_ASSET_TOKEN_ID,
                amount: new BigNumber(19530979944),
            }],
        },
    },
};
