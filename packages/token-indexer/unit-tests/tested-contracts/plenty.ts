import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { BurnProcessor } from '../../src/indexers/entrypoints/burn/processors/burn-processor';
import { Tzip7TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip7-transfer-processor';

export const PLENTY: TestContract = {
    address: 'KT1GRSvLoikDsXujKgZPsGLX8k8VvR2Tq95b',
    name: 'PLENTY',
    bigMapName: 'ledger',
    contractType: ContractType.FA1_2_like,
    burn: {
        expectedProcessor: BurnProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%address'] },
                { prim: 'nat', annots: ['%value'] },
            ],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { string: 'tz1Micmwn4MUNqoetgtoP3QK4GcZWVMdyrcS' },
                    { int: '100000000000000000000' },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: 'tz1Micmwn4MUNqoetgtoP3QK4GcZWVMdyrcS',
                amount: new BigNumber('100000000000000000000'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip7TransferProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%from'] },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%to'] },
                        { prim: 'nat', annots: ['%value'] },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '00002a86d0d3e0bad8004a5dba65cd46d73e892c3eac' },
                    {
                        prim: 'Pair',
                        args: [
                            { bytes: '01f5f864796ed4e26c71560f3fa17ca493e8b616a500' },
                            { int: '905088572067' },
                        ],
                    },
                ],
            },
            expected: [{
                fromAddress: 'tz1PWtaLXKiHXhXGvpuS8w4sVveNRKedTRSe',
                toAddress: 'KT1X1LgNkQShpF9nRLYw3Dgdy4qp38MX617z',
                tokenId: SINGLE_ASSET_TOKEN_ID,
                amount: new BigNumber(905088572067),
            }],
        },
    },
};
