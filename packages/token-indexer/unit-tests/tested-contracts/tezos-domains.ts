import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { contractAddresses } from '../../src/helpers/contract-addresses';
import { NFT_DEFAULT_AMOUNT } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { Tzip12TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip12-transfer-processor';
import {
    TezosDomainsLedgerProcessor,
} from '../../src/indexers/ledger/processors/incompatible/tezos-domains-ledger-processor';

export const TezosDomains: TestContract = {
    address: contractAddresses.TEZOS_DOMAINS,
    name: 'Tezos Domains',
    bigMapName: 'records',
    contractType: ContractType.FA2_like,
    ledger: {
        expectedProcessor: TezosDomainsLedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                { prim: 'bytes' },
                {
                    prim: 'pair',
                    args: [
                        {
                            prim: 'pair',
                            args: [
                                {
                                    prim: 'pair',
                                    args: [
                                        {
                                            prim: 'option',
                                            args: [
                                                { prim: 'address' },
                                            ],
                                        },
                                        {
                                            prim: 'map',
                                            args: [
                                                { prim: 'string' },
                                                { prim: 'bytes' },
                                            ],
                                        },
                                    ],
                                },
                                {
                                    prim: 'option',
                                    args: [
                                        { prim: 'bytes' },
                                    ],
                                },
                                {
                                    prim: 'map',
                                    args: [
                                        { prim: 'string' },
                                        { prim: 'bytes' },
                                    ],
                                },
                            ],
                        },
                        {
                            prim: 'pair',
                            args: [
                                { prim: 'nat', annots: ['%level'] },
                                { prim: 'address', annots: ['%owner'] },
                            ],
                        },
                        {
                            prim: 'option',
                            args: [
                                { prim: 'nat', annots: ['%tzip12_token_id'] },
                            ],
                        },
                    ],
                },
            ],
            annots: ['%records'],
        },
        sample: {
            input: {
                key: { bytes: '6275792e646f6d61696e732e74657a' },
                value: {
                    prim: 'Pair',
                    args: [
                        {
                            prim: 'Pair',
                            args: [
                                {
                                    prim: 'Pair',
                                    args: [
                                        {
                                            prim: 'Some',
                                            args: [
                                                { bytes: '019178a76f3c41a9541d2291cad37dd5fb96a6850500' },
                                            ],
                                        },
                                        [],
                                    ],
                                },
                                {
                                    prim: 'Pair',
                                    args: [
                                        {
                                            prim: 'None',
                                        },
                                        [],
                                    ],
                                },
                            ],
                        },
                        {
                            prim: 'Pair',
                            args: [
                                {
                                    prim: 'Pair',
                                    args: [
                                        { int: '2' },
                                        { bytes: '019178a76f3c41a9541d2291cad37dd5fb96a6850500' },
                                    ],
                                },
                                {
                                    prim: 'Pair',
                                    args: [
                                        { int: '1924' },
                                    ],
                                },
                            ],
                        },
                    ],
                },
            },
            expected: [{
                tokenId: new BigNumber(1924),
                ownerAddress: 'KT1Mqx5meQbhufngJnUAGEGpa4ZRxhPSiCgB',
                amount: NFT_DEFAULT_AMOUNT,
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip12TransferProcessor,
        schema: {
            prim: 'list',
            args: [{
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%from_'] },
                    {
                        prim: 'list',
                        args: [
                            {
                                prim: 'pair',
                                args: [
                                    { prim: 'address', annots: ['%to_'] },
                                    {
                                        prim: 'pair',
                                        args: [
                                            { prim: 'nat', annots: ['%token_id'] },
                                            { prim: 'nat', annots: ['%amount'] },
                                        ],
                                    },
                                ],
                            },
                        ],
                        annots: ['%txs'],
                    },
                ],
            }],
            annots: ['%transfer'],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { string: 'tz1ZaZwdrKqnmfpuZnmHeRKvtRSmvsvtvE1T' },
                    [
                        {
                            prim: 'Pair',
                            args: [
                                { string: 'tz1fEivXU93hFAthx3oT5xEqWFeh3TwZZmdz' },
                                {
                                    prim: 'Pair',
                                    args: [
                                        { int: '10987' },
                                        { int: '1' },
                                    ],
                                },
                            ],
                        },
                    ],
                ],
            }],
            expected: [{
                tokenId: new BigNumber('10987'),
                fromAddress: 'tz1ZaZwdrKqnmfpuZnmHeRKvtRSmvsvtvE1T',
                toAddress: 'tz1fEivXU93hFAthx3oT5xEqWFeh3TwZZmdz',
                amount: NFT_DEFAULT_AMOUNT,
            }],
        },
    },
};
