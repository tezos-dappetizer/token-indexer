import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { contractAddresses } from '../../src/helpers/contract-addresses';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { BurnProcessor } from '../../src/indexers/entrypoints/burn/processors/burn-processor';
import { MintProcessor } from '../../src/indexers/entrypoints/mint/processors/mint-processor';
import { Tzip7TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip7-transfer-processor';
import { SingleAssetLedgerProcessor } from '../../src/indexers/ledger/processors/incompatible/single-asset-ledger-processor';

export const kUSD: TestContract = {
    address: contractAddresses.K_USD,
    name: 'kUSD',
    bigMapName: 'balances',
    contractType: ContractType.FA1_2_like,
    ledger: {
        expectedProcessor: SingleAssetLedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                { prim: 'address' },
                {
                    prim: 'pair',
                    args: [
                        {
                            prim: 'map',
                            args: [
                                { prim: 'address' },
                                { prim: 'nat' },
                            ],
                            annots: ['%approvals'],
                        },
                        { prim: 'nat', annots: ['%balance'] },
                    ],
                },
            ],
            annots: ['%balances'],
        },
        sample: {
            input: {
                key: { bytes: '00008140b49bb87494d0a5c1a4f1bb2d8b84f0364b18' },
                value: {
                    prim: 'Pair',
                    args: [
                        [
                            {
                                prim: 'Elt',
                                args: [
                                    { bytes: '011615e137f0a86dadbe9c39093d87f13af5b627a300' },
                                    { int: '0' },
                                ],
                            },
                            {
                                prim: 'Elt',
                                args: [
                                    { bytes: '011a0fe8160084c07999a88b70ca233733aab4842d00' },
                                    { int: '0' },
                                ],
                            },
                        ],
                        { int: '6961240635806815844261' },
                    ],
                },
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                ownerAddress: 'tz1XRTNgXgWmR4PqxZpiB6HZJ8Siz3M2JAim',
                amount: new BigNumber('6961240635806815844261'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip7TransferProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%from'] },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%to'] },
                        { prim: 'nat', annots: ['%value'] },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '00004d697a8f557757bb4b1d37a963098d80adbdf88c' },
                    {
                        prim: 'Pair',
                        args: [
                            { bytes: '015eb4d94de864a991149836f88755d57bad9a1b5600' },
                            { int: '100690826292478537374' },
                        ],
                    },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: 'tz1ShM5gWyLSD997WV89nzZWxPXFGBMJ2yRw',
                toAddress: 'KT1HDXjPtjv7Y7XtJxrNc5rNjnegTi2ZzNfv',
                amount: new BigNumber('100690826292478537374'),
            }],
        },
    },
    mint: {
        expectedProcessor: MintProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%address'] },
                { prim: 'nat', annots: ['%value'] },
            ],
            annots: ['%mint'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '0000d0b7985f8f4821cdf75fbed2e51fb3e63c76a160' },
                    { int: '10000000000000000000' },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                toAddress: 'tz1efdBSPksB8RykXwUFG9rb5qLPbfYcwrtN',
                amount: new BigNumber('10000000000000000000'),
            }],
        },
    },
    burn: {
        expectedProcessor: BurnProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%address'] },
                { prim: 'nat', annots: ['%value'] },
            ],
            annots: ['%burn'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '0000c3814c6512fa6dd5d534ee20b187cddd5dc38921' },
                    { int: '450642612956356907220' },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: 'tz1dTmMLY7tan1HKfW5e3KxJcfZput6UuTH6',
                amount: new BigNumber('450642612956356907220'),
            }],
        },
    },
};
