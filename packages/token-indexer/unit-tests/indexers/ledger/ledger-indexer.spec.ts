import { BigMapInfo, BigMapUpdate, Contract, MichelsonSchema } from '@tezos-dappetizer/indexer';
import { asReadonly, getConstructorName } from '@tezos-dappetizer/utils';
import BigNumber from 'bignumber.js';
import { Counter } from 'prom-client';
import { Writable } from 'ts-essentials';
import { anything, instance, mock, when } from 'ts-mockito';

import { describeMember } from '../../../../../test-utilities/mocks';
import { TokenIndexerMetrics } from '../../../src/helpers/token-indexer-metrics';
import { ContractFeature, IndexedChangeType } from '../../../src/indexed-data';
import { CompositeLedgerProcessor, LedgerProcessorMatch } from '../../../src/indexers/ledger/composite-ledger-processor';
import { LedgerData, LedgerIndexer } from '../../../src/indexers/ledger/ledger-indexer';
import { LedgerProcessor } from '../../../src/indexers/ledger/ledger-processor';
import { WithFeatures } from '../../../src/indexers/partial-token-indexer';
import { TokenBigMapUpdateIndexingContext } from '../../../src/token-indexing-data';

describe(LedgerIndexer.name, () => {
    let target: LedgerIndexer;
    let compositeProcessor: CompositeLedgerProcessor;
    let metrics: Writable<TokenIndexerMetrics>;
    let processor: LedgerProcessor;

    beforeEach(() => {
        compositeProcessor = mock<CompositeLedgerProcessor>();
        metrics = {} as any;
        processor = mock<LedgerProcessor>();
        target = new LedgerIndexer(instance(compositeProcessor), metrics);

        metrics.processorUsage = instance(mock<Counter>());
    });

    describeMember<typeof target>('shouldIndex', () => {
        let contract: Contract;

        beforeEach(() => {
            contract = {
                bigMaps: 'mockedBigMaps' as any,
                parameterSchemas: {
                    entrypoints: {
                        transfer: {} as MichelsonSchema,
                    } as Record<string, MichelsonSchema>,
                },
            } as Contract;

            when(processor.contractFeatures).thenReturn([
                'SomeAssetType' as ContractFeature,
                'SomeFAType' as ContractFeature,
            ]);
        });

        it('should recognize that contract should be indexed with corresponding features', () => {
            when(compositeProcessor.findProcessor(contract.bigMaps, contract.address)).thenReturn({
                processor: instance(processor),
                ledgerBigMap: 'mockedBigMap' as any,
            } as LedgerProcessorMatch);

            const contractData = target.shouldIndex(contract) as WithFeatures<LedgerData>;

            expect(contractData.contractFeatures).toEqual([
                ContractFeature.Ledger,
                'SomeAssetType' as ContractFeature,
                'SomeFAType' as ContractFeature,
                getConstructorName(instance(processor)) as ContractFeature,
            ]);
            expect(contractData.data).toBeInstanceOf(LedgerData);
            expect(contractData.data.bigMap).toBe('mockedBigMap');
            expect(contractData.data.processor).toBe(instance(processor));
        });

        it('should recognize that contract should not be indexed - no processor', () => {
            when(compositeProcessor.findProcessor(anything(), anything())).thenReturn(null);

            const contractData = target.shouldIndex(contract);

            expect(contractData).toBe(false);
        });
    });

    describeMember<typeof target>('indexBigMapUpdate', () => {
        let bigMapUpdate: Writable<BigMapUpdate>;
        let context: TokenBigMapUpdateIndexingContext<LedgerData>;

        const act = () => Array.from(target.indexBigMapUpdate!(bigMapUpdate, context));

        beforeEach(() => {
            bigMapUpdate = {
                bigMap: { path: asReadonly(['mocked', 'big', 'map']) } as BigMapInfo,
                key: 'mockedKey' as any,
                value: 'mockedValue' as any,
            } as BigMapUpdate;
            context = {
                contractData: {
                    bigMap: bigMapUpdate.bigMap,
                    processor: instance(processor),
                } as LedgerData,
            } as TokenBigMapUpdateIndexingContext<LedgerData>;

            when(processor.deserializeRawUpdates(bigMapUpdate.key!, bigMapUpdate.value, context)).thenReturn([
                { tokenId: new BigNumber(1), ownerAddress: 'tk1', amount: new BigNumber(55) },
                { tokenId: new BigNumber(3), ownerAddress: 'tk2', amount: new BigNumber(122) },
            ]);
        });

        it('should index ledger', () => {
            const currentChanges = act();

            expect(currentChanges).toEqual([
                { type: IndexedChangeType.Ledger, tokenId: new BigNumber(1), ownerAddress: 'tk1', amount: new BigNumber(55) },
                { type: IndexedChangeType.Ledger, tokenId: new BigNumber(3), ownerAddress: 'tk2', amount: new BigNumber(122) },
            ]);
        });

        it('should not index ledger - unknown big map', () => {
            bigMapUpdate.bigMap = { path: asReadonly(['different', 'big', 'map']) } as BigMapInfo;

            const currentChanges = act();

            expect(currentChanges).toBeEmpty();
        });

        it('should not index ledger - no changes', () => {
            when(processor.deserializeRawUpdates(anything(), anything(), anything())).thenReturn([]);

            const currentChanges = act();

            expect(currentChanges).toBeEmpty();
        });
    });
});

describe(LedgerData.name, () => {
    it('should be serialized correctly', () => {
        const data = new LedgerData(new Date() as any, {
            lastKnownId: new BigNumber(123),
            path: asReadonly(['p1', 'p2']),
        } as BigMapInfo);

        const json = JSON.stringify(data);

        expect(json).toBe('{"processor":"Date","bigMap":{"lastKnownId":"123","path":["p1","p2"]}}');
    });
});
