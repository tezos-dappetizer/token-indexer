import { BigMapInfo, MichelsonSchema } from '@tezos-dappetizer/indexer';
import BigNumber from 'bignumber.js';
import { toArray } from 'iter-tools';

import { describeMember } from '../../../../../../test-utilities/mocks';
import { prepareContract, prepareUnknownBigMap } from '../../../../../../test-utilities/mocks/utils';
import { ContractType } from '../../../../src/indexers/contract-type-resolver';
import { RawLedgerUpdate } from '../../../../src/indexers/ledger/ledger-processor';
import {
    LedgerKey,
    Tzip12MultiAssetLedgerProcessor,
} from '../../../../src/indexers/ledger/processors/tzip12-multi-asset-ledger-processor';
import { TokenBigMapUpdateIndexingContext } from '../../../../src/token-indexing-data';

describe(Tzip12MultiAssetLedgerProcessor.name, () => {
    const target = new Tzip12MultiAssetLedgerProcessor();

    describeMember<typeof target>('shouldProcess', () => {
        it('should process when schema matches', () => {
            const bigMap = prepareValidBigMap();

            const currentShouldProcess = target.shouldProcess(bigMap, '');
            expect(currentShouldProcess).toBeTrue();
        });

        it('should not process when big map name does not match', () => {
            const bigMap = prepareUnknownBigMap();

            const currentShouldProcess = target.shouldProcess(bigMap, '');
            expect(currentShouldProcess).toBeFalse();
        });

        it('should not process when key schema does not match', () => {
            const bigMap = prepareInvalidKeySchemaBigMap();

            const currentShouldProcess = target.shouldProcess(bigMap, '');
            expect(currentShouldProcess).toBeFalse();
        });

        it('should not process when value schema does not match', () => {
            const bigMap = prepareInvalidValueSchemaBigMap();

            const currentShouldProcess = target.shouldProcess(bigMap, '');
            expect(currentShouldProcess).toBeFalse();
        });
    });

    describeMember<typeof target>('deserializeRawUpdatesFromNoAnnots', () => {
        it('should deserialize raw changes', () => {
            const key = {
                0: 'th1',
                1: new BigNumber(3),
            } as LedgerKey;

            const value = new BigNumber(34);

            const context = {} as TokenBigMapUpdateIndexingContext;

            const currentLedgerUpdates = toArray(target.deserializeRawUpdatesFromNoAnnots(key, value, context));

            const expectedLedgerUpdates = [
                {
                    tokenId: key[1],
                    ownerAddress: key[0],
                    amount: value,
                } as RawLedgerUpdate,
            ];
            expect(currentLedgerUpdates).toEqual(expectedLedgerUpdates);
        });
    });

    describeMember<typeof target>('resolveContractType', () => {
        it('should resolve contract type', () => {
            const contract = prepareContract([
                prepareValidBigMap(),
                prepareUnknownBigMap(),
            ]);

            const currentContractType = target.resolveContractType(contract);

            const expectedContractType = ContractType.FA2_like;
            expect(currentContractType).toEqual(expectedContractType);
        });

        it('should not resolve contract type - unknown big map name', () => {
            const contract = prepareContract([
                prepareUnknownBigMap(),
                prepareUnknownBigMap(),
            ]);

            const currentContractType = target.resolveContractType(contract);
            expect(currentContractType).toBeNull();
        });

        it('should not resolve contract type - incompatible schema', () => {
            const contract = prepareContract([
                prepareInvalidKeySchemaBigMap(),
                prepareInvalidValueSchemaBigMap(),
                prepareUnknownBigMap(),
            ]);

            const currentContractType = target.resolveContractType(contract);
            expect(currentContractType).toBeNull();
        });
    });
});

function prepareValidBigMap(): BigMapInfo {
    return {
        name: 'ledger',
        keySchema: new MichelsonSchema({
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%owner'] },
                { prim: 'nat', annots: ['%token_id'] },
            ],
        }),
        valueSchema: new MichelsonSchema({
            prim: 'nat', annots: ['%amount'],
        }),
    } as BigMapInfo;
}

function prepareInvalidKeySchemaBigMap(): BigMapInfo {
    return {
        name: 'ledger',
        keySchema: new MichelsonSchema({
            prim: 'pair',
            args: [
                { prim: 'nat', annots: ['%something'] },
                { prim: 'nat', annots: ['%token_id'] },
            ],
        }),
        valueSchema: new MichelsonSchema({
            prim: 'nat', annots: ['%amount'],
        }),
    } as BigMapInfo;
}

function prepareInvalidValueSchemaBigMap(): BigMapInfo {
    return {
        name: 'ledger',
        keySchema: new MichelsonSchema({
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%owner'] },
                { prim: 'nat', annots: ['%token_id'] },
            ],
        }),
        valueSchema: new MichelsonSchema({
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%something'] },
                {
                    prim: 'map',
                    args: [
                        { prim: 'address' },
                        { prim: 'nat' },
                    ],
                    annots: ['%allowances'],
                },
            ],
        }),
    } as BigMapInfo;
}
