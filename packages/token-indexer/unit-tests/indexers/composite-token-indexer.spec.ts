import { BigMapUpdate, Contract, LazyContract, TransactionParameter } from '@tezos-dappetizer/indexer';
import BigNumber from 'bignumber.js';
import { Writable } from 'ts-essentials';
import { anything, capture, instance, mock, verify, when } from 'ts-mockito';

import { describeMember, expectToThrow, expectToThrowAsync } from '../../../../test-utilities/mocks';
import { TokenIndexerInternalConfig } from '../../src/config/token-indexer-internal-config';
import { IndexedChange, IndexedChangeType, IndexedContract } from '../../src/indexed-data';
import { CompositeContractTypeResolver } from '../../src/indexers/composite-contract-type-resolver';
import { CompositeContractData, CompositeTokenIndexer } from '../../src/indexers/composite-token-indexer';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { PartialIndexedChange, PartialTokenIndexer } from '../../src/indexers/partial-token-indexer';
import { ContractMetadata } from '../../src/metadata/contract/contract-metadata';
import { ContractMetadataProvider } from '../../src/metadata/contract/contract-metadata-provider';
import { TokenBigMapUpdateIndexingContext, TokenTransactionIndexingContext } from '../../src/token-indexing-data';

describe(CompositeTokenIndexer.name, () => {
    let target: CompositeTokenIndexer;
    let tokenIndexer1: PartialTokenIndexer;
    let tokenIndexer2: PartialTokenIndexer;
    let tokenIndexer3: PartialTokenIndexer;
    let contractMetadataProvider: ContractMetadataProvider;
    let contractTypeResolver: CompositeContractTypeResolver;
    let config: Writable<TokenIndexerInternalConfig>;
    let metadata: ContractMetadata;

    const createTarget = () => new CompositeTokenIndexer(
        [instance(tokenIndexer1), instance(tokenIndexer2), instance(tokenIndexer3)],
        instance(contractMetadataProvider),
        instance(contractTypeResolver),
        config,
    );

    beforeEach(() => {
        [tokenIndexer1, tokenIndexer2, tokenIndexer3, contractMetadataProvider, contractTypeResolver]
            = [mock(), mock(), mock(), mock(), mock(), mock(), mock()];
        config = {} as typeof config;
        metadata = 'mockedMetadata' as any;

        when(tokenIndexer1.name).thenReturn('indexer1');
        when(tokenIndexer2.name).thenReturn('indexer2');
        when(tokenIndexer3.name).thenReturn('indexer3');

        target = createTarget();
    });

    describe('constructor', () => {
        it('should throw if duplicate token indexer name', () => {
            when(tokenIndexer1.name).thenReturn('wtf');
            when(tokenIndexer2.name).thenReturn('wtf');

            const error = expectToThrow(createTarget);

            expect(error.message).toInclude(`'wtf'`);
        });
    });

    describeMember<typeof target>('shouldIndex', () => {
        let lazyContract: LazyContract;
        let contract: Contract;

        const act = async () => target.shouldIndex(instance(lazyContract));

        beforeEach(() => {
            lazyContract = mock();
            contract = {
                address: 'mockedAddr',
                abstraction: 'mockedAbstraction' as any,
            } as Contract;

            when(lazyContract.getContract()).thenResolve(contract);
            when(contractTypeResolver.resolveContractType(contract)).thenReturn(ContractType.FA2_like);
            when(contractMetadataProvider.get(anything())).thenResolve(metadata);

            when(tokenIndexer1.shouldIndex(contract)).thenReturn({
                contractFeatures: ['mockedFeature1' as any],
                data: 'mockedData1',
            });
            when(tokenIndexer2.shouldIndex(contract)).thenReturn(false);
            when(tokenIndexer3.shouldIndex(contract)).thenReturn({
                contractFeatures: ['mockedFeature3_1' as any, 'mockedFeature3_2' as any],
                data: 'mockedData3',
            });
        });

        it.each([true, false])('should return corresponding contract data and features if selective indexing %s', async selectiveIndexing => {
            if (selectiveIndexing) {
                when(lazyContract.config).thenReturn({ name: 'Foo' } as typeof lazyContract.config);
                config.contractNames = ['Foo'];
            }

            const contractData = await act() as CompositeContractData;

            expect(contractData.tokenIndexersData).toEqual({
                indexer1: 'mockedData1',
                indexer3: 'mockedData3',
            });
            expect(contractData.indexedContract.features).toEqual([ContractType.FA2_like, 'mockedFeature1', 'mockedFeature3_1', 'mockedFeature3_2']);
            expect(contractData.indexedContract.address).toEqual(contract.address);
            expect(contractData.indexedContract.metadata).toBe(metadata);
        });

        it.each([
            ['not matched', 'NotFoo'],
            ['null', null],
        ])('should return false if selective indexing enabled and contract name is %s', async (_desc, contractName) => {
            when(lazyContract.config).thenReturn({ name: contractName } as typeof lazyContract.config);
            config.contractNames = ['Foo'];

            const contractData = await act();

            expect(contractData).toBeFalse();
            verify(lazyContract.getContract()).never();
            verify(contractTypeResolver.resolveContractType(anything())).never();
        });

        it('should return false if no contract type resolved', async () => {
            when(contractTypeResolver.resolveContractType(contract)).thenReturn(null);

            const contractData = await act();

            expect(contractData).toBeFalse();
            verify(tokenIndexer1.shouldIndex(anything())).never();
            verify(tokenIndexer2.shouldIndex(anything())).never();
            verify(tokenIndexer3.shouldIndex(anything())).never();
        });

        it('should throw if no contract type resolved but no indexer matches', async () => {
            when(tokenIndexer1.shouldIndex(contract)).thenReturn(false);
            when(tokenIndexer3.shouldIndex(contract)).thenReturn(false);

            const error = await expectToThrowAsync(act);

            expect(error.message).toIncludeMultiple([`'mockedAddr'`, ContractType.FA2_like]);
        });
    });

    describeMember<typeof target>('indexTransaction', () => {
        let transactionParam: TransactionParameter;
        let contract: IndexedContract;
        let indexingContext: TokenTransactionIndexingContext<CompositeContractData>;

        const act = () => target.indexTransaction(transactionParam, null!, indexingContext);

        beforeEach(() => {
            transactionParam = 'mockedParam' as any;
            contract = 'mockedContract' as any;

            indexingContext = {
                data: { changes: [] as IndexedChange[] },
                contractData: {
                    tokenIndexersData: {
                        indexer1: 'mockedIndexerData1',
                        indexer2: 'mockedIndexerData2',
                        indexer3: 'mockedIndexerData3',
                    } as Record<string, unknown>,
                    indexedContract: contract,
                },
                operationGroup: { hash: 'haha' },
            } as TokenTransactionIndexingContext<CompositeContractData>;

            when(tokenIndexer1.indexTransaction!(transactionParam, anything())).thenReturn([
                { type: IndexedChangeType.Transfer, amount: new BigNumber(1) } as PartialIndexedChange,
            ]);
            when(tokenIndexer2.indexTransaction!(transactionParam, anything())).thenReturn([
                { type: IndexedChangeType.Mint, toAddress: 'toAddr2' } as PartialIndexedChange,
                { type: IndexedChangeType.Burn, fromAddress: 'fromAddr3' } as PartialIndexedChange,
            ]);
            when(tokenIndexer3.indexTransaction!(transactionParam, anything())).thenReturn([
                { type: IndexedChangeType.Ledger, ownerAddress: 'ownerAddr3' } as PartialIndexedChange,
            ]);
        });

        it('should transform changes from all token indexers', () => {
            act();

            expect(indexingContext.data.changes).toEqual<Partial<IndexedChange>[]>([
                { contract, operationGroupHash: 'haha', type: IndexedChangeType.Transfer, amount: new BigNumber(1) },
                { contract, operationGroupHash: 'haha', type: IndexedChangeType.Mint, toAddress: 'toAddr2' },
                { contract, operationGroupHash: 'haha', type: IndexedChangeType.Burn, fromAddress: 'fromAddr3' },
                { contract, operationGroupHash: 'haha', type: IndexedChangeType.Ledger, ownerAddress: 'ownerAddr3' },
            ]);
            verifyIndexTransaction(tokenIndexer1, 'mockedIndexerData1');
            verifyIndexTransaction(tokenIndexer2, 'mockedIndexerData2');
            verifyIndexTransaction(tokenIndexer3, 'mockedIndexerData3');

            function verifyIndexTransaction(tokenIndexer: PartialTokenIndexer, expectedContractData: unknown) {
                const receivedContext = capture(tokenIndexer.indexTransaction!).first()[1];
                expect(receivedContext.contractData).toBe(expectedContractData);
                expect(receivedContext.operationGroup.hash).toBe('haha'); // Arbitrary property to check that entire context was copied.
            }
        });

        it('should not call token indexer if no method or no related contract data', () => {
            indexingContext = {
                ...indexingContext,
                contractData: {
                    tokenIndexersData: {
                        indexer1: 'mockedIndexerData1',
                        indexer3: 'mockedIndexerData3',
                    } as Record<string, unknown>,
                    indexedContract: contract,
                },
            };
            when(tokenIndexer1.indexTransaction).thenReturn(undefined);
            when(tokenIndexer3.indexTransaction).thenReturn(undefined);

            act();

            expect(indexingContext.data.changes).toBeEmpty();
            verify(tokenIndexer2.indexTransaction!(anything(), anything())).never();
        });
    });

    describeMember<typeof target>('indexBigMapUpdate', () => {
        let bigMapUpdate: BigMapUpdate;
        let contract: IndexedContract;
        let indexingContext: TokenBigMapUpdateIndexingContext<CompositeContractData>;

        const act = () => target.indexBigMapUpdate(bigMapUpdate, null!, indexingContext);

        beforeEach(() => {
            bigMapUpdate = 'mockedBigMapUpdate' as any;
            contract = 'mockedContract' as any;

            indexingContext = {
                data: { changes: [] as IndexedChange[] },
                contractData: {
                    tokenIndexersData: {
                        indexer1: 'mockedIndexerData1',
                        indexer2: 'mockedIndexerData2',
                        indexer3: 'mockedIndexerData3',
                    } as Record<string, unknown>,
                    indexedContract: contract,
                },
                operationGroup: { hash: 'haha' },
            } as TokenBigMapUpdateIndexingContext<CompositeContractData>;

            when(tokenIndexer1.indexBigMapUpdate!(bigMapUpdate, anything())).thenReturn([
                { type: IndexedChangeType.Transfer, amount: new BigNumber(1) } as PartialIndexedChange,
            ]);
            when(tokenIndexer2.indexBigMapUpdate!(bigMapUpdate, anything())).thenReturn([
                { type: IndexedChangeType.Mint, toAddress: 'toAddr2' } as PartialIndexedChange,
                { type: IndexedChangeType.Burn, fromAddress: 'fromAddr3' } as PartialIndexedChange,
            ]);
            when(tokenIndexer3.indexBigMapUpdate!(bigMapUpdate, anything())).thenReturn([
                { type: IndexedChangeType.Ledger, ownerAddress: 'ownerAddr3' } as PartialIndexedChange,
            ]);
        });

        it('should transform changes from all token indexers', () => {
            act();

            expect(indexingContext.data.changes).toEqual<Partial<IndexedChange>[]>([
                { contract, operationGroupHash: 'haha', type: IndexedChangeType.Transfer, amount: new BigNumber(1) },
                { contract, operationGroupHash: 'haha', type: IndexedChangeType.Mint, toAddress: 'toAddr2' },
                { contract, operationGroupHash: 'haha', type: IndexedChangeType.Burn, fromAddress: 'fromAddr3' },
                { contract, operationGroupHash: 'haha', type: IndexedChangeType.Ledger, ownerAddress: 'ownerAddr3' },
            ]);
            verifyIndexBigMapUpdate(tokenIndexer1, 'mockedIndexerData1');
            verifyIndexBigMapUpdate(tokenIndexer2, 'mockedIndexerData2');
            verifyIndexBigMapUpdate(tokenIndexer3, 'mockedIndexerData3');

            function verifyIndexBigMapUpdate(tokenIndexer: PartialTokenIndexer, expectedContractData: unknown) {
                const receivedContext = capture(tokenIndexer.indexBigMapUpdate!).first()[1];
                expect(receivedContext.contractData).toBe(expectedContractData);
                expect(receivedContext.operationGroup.hash).toBe('haha'); // Arbitrary property to check that entire context was copied.
            }
        });

        it('should not call token indexer if no method or no related contract data', () => {
            indexingContext = {
                ...indexingContext,
                contractData: {
                    tokenIndexersData: {
                        indexer1: 'mockedIndexerData1',
                        indexer3: 'mockedIndexerData3',
                    } as Record<string, unknown>,
                    indexedContract: contract,
                },
            };
            when(tokenIndexer1.indexBigMapUpdate).thenReturn(undefined);
            when(tokenIndexer3.indexBigMapUpdate).thenReturn(undefined);

            act();

            expect(indexingContext.data.changes).toBeEmpty();
            verify(tokenIndexer2.indexBigMapUpdate!(anything(), anything())).never();
        });
    });
});
