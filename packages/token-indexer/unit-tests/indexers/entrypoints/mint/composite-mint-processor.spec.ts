import { MichelsonSchema, TransactionParameter } from '@tezos-dappetizer/indexer';
import { MarkRequired } from 'ts-essentials';

import { CompositeMintProcessor } from '../../../../src/indexers/entrypoints/mint/composite-mint-processor';
import { TokenTransactionIndexingContext } from '../../../../src/token-indexing-data';
import { testContracts } from '../../../tested-contracts';
import { TestContract } from '../../../tested-contracts/test-contract';

describe(CompositeMintProcessor.name, () => {
    const target = new CompositeMintProcessor();

    describe('should match schema and deserialize sample value correctly', () => {
        for (const testContract of testContracts.filter(c => c.mint) as MarkRequired<TestContract, 'mint'>[]) {
            it(testContract.name, () => {
                const schema = new MichelsonSchema(testContract.mint.schema);
                const processor = target.findProcessor(schema, testContract.address);

                expect(processor?.constructor.name).toBe(testContract.mint.expectedProcessor?.name);

                if (testContract.mint.sample) {
                    const transactionParameter = {
                        value: {
                            michelson: testContract.mint.sample.input,
                            convert: () => {
                                return schema.execute<any>(testContract.mint.sample!.input);
                            },
                        },
                    } as TransactionParameter;
                    const context = {
                        operationWithResult: { sourceAddress: 'operation result parent source' },
                        mainOperation: { sourceAddress: 'operation source' },
                    } as TokenTransactionIndexingContext;

                    const param = Array.from(processor!.deserializeRawChanges(transactionParameter, context));

                    expect(param).toEqual(testContract.mint.sample.expected);
                }
            });
        }
    });
});
