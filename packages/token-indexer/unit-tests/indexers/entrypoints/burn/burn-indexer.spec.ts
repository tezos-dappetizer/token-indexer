import { Contract, MichelsonSchema, TransactionParameter } from '@tezos-dappetizer/indexer';
import { getConstructorName } from '@tezos-dappetizer/utils';
import BigNumber from 'bignumber.js';
import { Counter } from 'prom-client';
import { anything, instance, mock, when } from 'ts-mockito';

import { describeMember } from '../../../../../../test-utilities/mocks';
import { TokenIndexerMetrics } from '../../../../src/helpers/token-indexer-metrics';
import { ContractFeature, IndexedChangeType } from '../../../../src/indexed-data';
import { BurnIndexer } from '../../../../src/indexers/entrypoints/burn/burn-indexer';
import { CompositeBurnProcessor } from '../../../../src/indexers/entrypoints/burn/composite-burn-processor';
import { RawBurn } from '../../../../src/indexers/entrypoints/burn/raw-burn';
import { EntrypointData } from '../../../../src/indexers/entrypoints/entrypoint-indexer';
import { EntrypointProcessor } from '../../../../src/indexers/entrypoints/entrypoint-processor';
import { IncompatibleContractReporter } from '../../../../src/indexers/incompatible-contract-reporter';
import { PartialIndexedChange } from '../../../../src/indexers/partial-token-indexer';
import { TokenTransactionIndexingContext } from '../../../../src/token-indexing-data';

describe(BurnIndexer.name, () => {
    let target: BurnIndexer;
    let compositeProcessor: CompositeBurnProcessor;
    let reporter: IncompatibleContractReporter;
    let metrics: TokenIndexerMetrics;
    let processor: EntrypointProcessor<RawBurn>;

    beforeEach(() => {
        [compositeProcessor, reporter, metrics, processor] = [mock(), mock(), mock(), mock()];
        target = new BurnIndexer(instance(compositeProcessor), instance(reporter), instance(metrics));

        when(metrics.processorUsage).thenReturn(instance(mock<Counter>()));
        when(compositeProcessor.findProcessor(anything(), anything())).thenReturn(instance(processor));
    });

    describeMember<typeof target>('createIndexedChange', () => {
        it('should create indexed change', () => {
            const rawBurn = {
                tokenId: new BigNumber(1),
                fromAddress: 'tk1',
                amount: new BigNumber(55),
            } as RawBurn;

            const currentChange = target.createIndexedChange(rawBurn);

            const expectedChange = {
                type: IndexedChangeType.Burn,
                tokenId: rawBurn.tokenId,
                fromAddress: rawBurn.fromAddress,
                amount: rawBurn.amount,
            };

            expect(currentChange).toEqual(expectedChange);
        });
    });

    describeMember<typeof target>('shouldIndex', () => {
        it('should recognize that contract should be indexed with corresponding features', () => {
            when(processor.contractFeatures).thenReturn([
                'SomeAssetType' as ContractFeature,
                'SomeFAType' as ContractFeature,
            ]);

            const processorName = getConstructorName(instance(processor));

            const contract = {
                parameterSchemas: {
                    entrypoints: {
                        burn: {} as MichelsonSchema,
                    } as Record<string, MichelsonSchema>,
                },
            } as Contract;

            const currentShouldIndex: any = target.shouldIndex(contract);

            const expectedContractFeatures = [
                ContractFeature.Burn,
                'SomeAssetType' as ContractFeature,
                'SomeFAType' as ContractFeature,
                processorName as ContractFeature,
            ];

            expect(currentShouldIndex.contractFeatures).toEqual(expectedContractFeatures);
            expect(currentShouldIndex.data.toJSON()).toEqual({ entrypoint: 'burn', processor: processorName });
        });

        it('should recognize that contract should not be indexed - no entrypoint', () => {
            const contract = {
                parameterSchemas: {
                    entrypoints: {
                        custom_burn: {} as MichelsonSchema,
                    } as Record<string, MichelsonSchema>,
                },
            } as Contract;

            expect(target.shouldIndex(contract)).toBe(false);
        });

        it('should recognize that contract should not be indexed - no processor', () => {
            when(compositeProcessor.findProcessor(anything(), anything()))
                .thenReturn(null);

            const contract = {
                parameterSchemas: {
                    entrypoints: {
                        burn: {} as MichelsonSchema,
                    } as Record<string, MichelsonSchema>,
                },
            } as Contract;

            expect(target.shouldIndex(contract)).toBe(false);
        });
    });

    describeMember<typeof target>('indexTransaction', () => {
        it('should index entrypoint', () => {
            const rawBurns: RawBurn[] = [
                {
                    tokenId: new BigNumber(1),
                    fromAddress: 'tk1',
                    amount: new BigNumber(55),
                }, {
                    tokenId: new BigNumber(3),
                    fromAddress: 'tk2',
                    amount: new BigNumber(122),
                },
            ];

            when(processor.deserializeRawChanges(anything(), anything())).thenReturn(rawBurns);

            const transactionParameter = {
                entrypoint: 'burn',
            } as TransactionParameter;

            const context = {
                contractData: {
                    entrypoint: 'burn',
                    processor: instance(processor),
                } as EntrypointData<RawBurn>,
            } as TokenTransactionIndexingContext<EntrypointData<RawBurn>>;

            const currentChanges = Array.from(target.indexTransaction(transactionParameter, context));

            const expectedChanges: PartialIndexedChange[] = rawBurns.map(c => {
                return {
                    type: IndexedChangeType.Burn,
                    tokenId: c.tokenId,
                    fromAddress: c.fromAddress,
                    amount: c.amount,
                } as PartialIndexedChange;
            });

            expect(currentChanges).toEqual(expectedChanges);
        });

        it('should not index entrypoint - unknown entrypoint', () => {
            const transactionParameter = {
                entrypoint: 'custom_burn',
            } as TransactionParameter;

            const context = {
                contractData: {
                    entrypoint: 'burn',
                } as EntrypointData<RawBurn>,
            } as TokenTransactionIndexingContext<EntrypointData<RawBurn>>;

            const currentChanges = Array.from(target.indexTransaction(transactionParameter, context));
            expect(currentChanges).toBeEmpty();
        });

        it('should not index entrypoint - no changes', () => {
            when(processor.deserializeRawChanges(anything(), anything())).thenReturn([]);

            const transactionParameter = {
                entrypoint: 'burn',
            } as TransactionParameter;

            const context = {
                contractData: {
                    entrypoint: 'burn',
                    processor: instance(processor),
                } as EntrypointData<RawBurn>,
            } as TokenTransactionIndexingContext<EntrypointData<RawBurn>>;

            const currentChanges = Array.from(target.indexTransaction(transactionParameter, context));
            expect(currentChanges).toBeEmpty();
        });
    });
});
