import BigNumber from 'bignumber.js';

import { describeMember } from '../../../../../../../test-utilities/mocks';
import { NFT_DEFAULT_AMOUNT, SINGLE_ASSET_TOKEN_ID } from '../../../../../src/helpers/token-constants';
import { BurnProcessor } from '../../../../../src/indexers/entrypoints/burn/processors/burn-processor';
import { shouldDeserializeIf, shouldProcessIf, shouldThrowIf } from '../../entrypoint-processor-test-helpers';

describe(BurnProcessor.name, () => {
    const target = new BurnProcessor();

    describeMember<typeof target>('shouldProcess', () => {
        shouldProcessIf('schema corresponds', target, {
            parameterSchema: {
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%something'] },
                    { prim: 'address', annots: ['%from'] },
                ],
            },
            expectedProcessing: true,
        });

        shouldProcessIf('schema is missing "from" field', target, {
            parameterSchema: {
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%something'] },
                    { prim: 'address', annots: ['%to'] },
                ],
            },
            expectedProcessing: false,
        });
    });

    describeMember<typeof target>('deserializeRawChanges', () => {
        shouldThrowIf('missing parameter "from" field', target, {
            transactionParam: { amount: new BigNumber(122) },
        });

        shouldDeserializeIf('multi asset contract', target, {
            transactionParam: {
                from: 'mockedAddr',
                amount: new BigNumber(122),
                token_id: new BigNumber(2),
            },
            expectedChanges: [{
                tokenId: new BigNumber(2),
                fromAddress: 'mockedAddr',
                amount: new BigNumber(122),
            }],
        });

        shouldDeserializeIf('single or nft asset contract', target, {
            transactionParam: {
                from: 'mockedAddr',
            },
            expectedChanges: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: 'mockedAddr',
                amount: NFT_DEFAULT_AMOUNT,
            }],
        });
    });
});
