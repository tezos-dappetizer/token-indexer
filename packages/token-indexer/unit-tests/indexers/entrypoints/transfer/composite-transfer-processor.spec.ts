import { MichelsonSchema, TransactionParameter } from '@tezos-dappetizer/indexer';
import { MarkRequired } from 'ts-essentials';

import { CompositeTransferProcessor } from '../../../../src/indexers/entrypoints/transfer/composite-transfer-processor';
import { TokenTransactionIndexingContext } from '../../../../src/token-indexing-data';
import { testContracts } from '../../../tested-contracts';
import { TestContract } from '../../../tested-contracts/test-contract';

describe(CompositeTransferProcessor.name, () => {
    const target = new CompositeTransferProcessor();

    describe('should match schema and deserialize sample value correctly', () => {
        for (const testContract of testContracts.filter(c => c.transfer) as MarkRequired<TestContract, 'transfer'>[]) {
            it(testContract.name, () => {
                const schema = new MichelsonSchema(testContract.transfer.schema);
                const processor = target.findProcessor(schema, testContract.address);

                expect(processor?.constructor.name).toBe(testContract.transfer.expectedProcessor?.name);

                if (testContract.transfer.sample) {
                    const transactionParameter = {
                        value: { michelson: testContract.transfer.sample.input },
                    } as TransactionParameter;
                    const context = {
                        operationWithResult: { sourceAddress: 'operation result parent source' },
                        mainOperation: { sourceAddress: 'operation source' },
                    } as TokenTransactionIndexingContext;

                    const param = Array.from(processor!.deserializeRawChanges(transactionParameter, context));

                    expect(param).toEqual(testContract.transfer.sample.expected);
                }
            });
        }
    });
});
