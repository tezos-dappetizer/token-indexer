import { Contract, MichelsonSchema, TransactionParameter } from '@tezos-dappetizer/indexer';
import { getConstructorName } from '@tezos-dappetizer/utils';
import BigNumber from 'bignumber.js';
import { anything, instance, mock, when } from 'ts-mockito';

import { describeMember } from '../../../../../../test-utilities/mocks';
import { TokenIndexerMetrics } from '../../../../src/helpers/token-indexer-metrics';
import { ContractFeature, IndexedChangeType } from '../../../../src/indexed-data';
import { EntrypointData } from '../../../../src/indexers/entrypoints/entrypoint-indexer';
import { EntrypointProcessor } from '../../../../src/indexers/entrypoints/entrypoint-processor';
import { CompositeTransferProcessor } from '../../../../src/indexers/entrypoints/transfer/composite-transfer-processor';
import { TransferIndexer } from '../../../../src/indexers/entrypoints/transfer/transfer-indexer';
import { RawTransfer } from '../../../../src/indexers/entrypoints/transfer/transfer-processor-no-annots';
import { IncompatibleContractReporter } from '../../../../src/indexers/incompatible-contract-reporter';
import { PartialIndexedChange } from '../../../../src/indexers/partial-token-indexer';
import { TokenTransactionIndexingContext } from '../../../../src/token-indexing-data';

describe(TransferIndexer.name, () => {
    let target: TransferIndexer;
    let compositeProcessor: CompositeTransferProcessor;
    let reporter: IncompatibleContractReporter;
    let metrics: TokenIndexerMetrics;
    let processor: EntrypointProcessor<RawTransfer>;

    beforeEach(() => {
        [compositeProcessor, reporter, metrics, processor] = [mock(), mock(), mock(), mock()];
        target = new TransferIndexer(instance(compositeProcessor), instance(reporter), instance(metrics));

        when(metrics.processorUsage).thenReturn(instance(mock()));
        when(compositeProcessor.findProcessor(anything(), anything())).thenReturn(instance(processor));
    });

    describeMember<typeof target>('createIndexedChange', () => {
        it('should create indexed change', () => {
            const rawTransfer = {
                tokenId: new BigNumber(1),
                fromAddress: 'tk1',
                toAddress: 'tk2',
                amount: new BigNumber(55),
            } as RawTransfer;

            const currentChange = target.createIndexedChange(rawTransfer);

            const expectedChange = {
                type: IndexedChangeType.Transfer,
                tokenId: rawTransfer.tokenId,
                fromAddress: rawTransfer.fromAddress,
                toAddress: rawTransfer.toAddress,
                amount: rawTransfer.amount,
            };

            expect(currentChange).toEqual(expectedChange);
        });
    });

    describeMember<typeof target>('shouldIndex', () => {
        it('should recognize that contract should be indexed with corresponding features', () => {
            when(processor.contractFeatures).thenReturn([
                'SomeAssetType' as ContractFeature,
                'SomeFAType' as ContractFeature,
            ]);

            const processorName = getConstructorName(instance(processor));

            const contract = {
                parameterSchemas: {
                    entrypoints: {
                        transfer: {} as MichelsonSchema,
                    } as Record<string, MichelsonSchema>,
                },
            } as Contract;

            const currentShouldIndex: any = target.shouldIndex(contract);

            const expectedContractFeatures = [
                ContractFeature.Transfer,
                'SomeAssetType' as ContractFeature,
                'SomeFAType' as ContractFeature,
                processorName as ContractFeature,
            ];

            expect(currentShouldIndex.contractFeatures).toEqual(expectedContractFeatures);
            expect(currentShouldIndex.data.toJSON()).toEqual({ entrypoint: 'transfer', processor: processorName });
        });

        it('should recognize that contract should not be indexed - no entrypoint', () => {
            const contract = {
                parameterSchemas: {
                    entrypoints: {
                        custom_transfer: {} as MichelsonSchema,
                    } as Record<string, MichelsonSchema>,
                },
            } as Contract;

            const currentShouldIndex = target.shouldIndex(contract);

            expect(currentShouldIndex).toBe(false);
        });

        it('should recognize that contract should not be indexed - no processor', () => {
            when(compositeProcessor.findProcessor(anything(), anything())).thenReturn(null);

            const contract = {
                parameterSchemas: {
                    entrypoints: {
                        transfer: {} as MichelsonSchema,
                    } as Record<string, MichelsonSchema>,
                },
            } as Contract;

            const currentShouldIndex = target.shouldIndex(contract);

            expect(currentShouldIndex).toBe(false);
        });
    });

    describeMember<typeof target>('shouldIndex', () => {
        it('should index entrypoint', () => {
            const rawTransfers: RawTransfer[] = [
                {
                    tokenId: new BigNumber(1),
                    fromAddress: 'tk1',
                    toAddress: 'tk2',
                    amount: new BigNumber(55),
                }, {
                    tokenId: new BigNumber(3),
                    fromAddress: 'tk4',
                    toAddress: 'tk2',
                    amount: new BigNumber(122),
                },
            ];

            when(processor.deserializeRawChanges(anything(), anything()))
                .thenReturn(rawTransfers);

            const transactionParameter = {
                entrypoint: 'transfer',
            } as TransactionParameter;

            const context = {
                contractData: {
                    entrypoint: 'transfer',
                    processor: instance(processor),
                } as EntrypointData<RawTransfer>,
            } as TokenTransactionIndexingContext<EntrypointData<RawTransfer>>;

            const currentChanges = Array.from(target.indexTransaction(transactionParameter, context));

            const expectedChanges: PartialIndexedChange[] = rawTransfers.map(c => {
                return {
                    type: IndexedChangeType.Transfer,
                    tokenId: c.tokenId,
                    fromAddress: c.fromAddress,
                    toAddress: c.toAddress,
                    amount: c.amount,
                } as PartialIndexedChange;
            });

            expect(currentChanges).toEqual(expectedChanges);
        });

        it('should not index entrypoint - unknown entrypoint', () => {
            const transactionParameter = {
                entrypoint: 'custom_transfer',
            } as TransactionParameter;

            const context = {
                contractData: {
                    entrypoint: 'transfer',
                } as EntrypointData<RawTransfer>,
            } as TokenTransactionIndexingContext<EntrypointData<RawTransfer>>;

            const currentChanges = Array.from(target.indexTransaction(transactionParameter, context));

            expect(currentChanges).toBeEmpty();
        });

        it('should not index entrypoint - no changes', () => {
            when(processor.deserializeRawChanges(anything(), anything()))
                .thenReturn([]);

            const transactionParameter = {
                entrypoint: 'transfer',
            } as TransactionParameter;

            const context = {
                contractData: {
                    entrypoint: 'transfer',
                    processor: instance(processor),
                } as EntrypointData<RawTransfer>,
            } as TokenTransactionIndexingContext<EntrypointData<RawTransfer>>;

            const currentChanges = Array.from(target.indexTransaction(transactionParameter, context));
            expect(currentChanges).toBeEmpty();
        });
    });
});
