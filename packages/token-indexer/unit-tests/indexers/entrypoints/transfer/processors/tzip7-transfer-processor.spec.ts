import BigNumber from 'bignumber.js';
import { toArray } from 'iter-tools';

import { describeMember } from '../../../../../../../test-utilities/mocks';
import { SINGLE_ASSET_TOKEN_ID } from '../../../../../src/helpers/token-constants';
import { ContractType } from '../../../../../src/indexers/contract-type-resolver';
import {
    Tzip7TransferProcessor,
} from '../../../../../src/indexers/entrypoints/transfer/processors/tzip7-transfer-processor';
import { RawTransfer } from '../../../../../src/indexers/entrypoints/transfer/transfer-processor-no-annots';
import { shouldProcessIf, shouldResolveTypeIf } from '../../entrypoint-processor-test-helpers';

describe(Tzip7TransferProcessor.name, () => {
    const target = new Tzip7TransferProcessor();

    describeMember<typeof target>('shouldProcess', () => {
        shouldProcessIf('schema matches', target, {
            parameterSchema: {
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%from'] },
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'address', annots: ['%to'] },
                            { prim: 'nat', annots: ['%value'] },
                        ],
                    },
                ],
            },
            expectedProcessing: true,
        });

        shouldProcessIf('schema does not match', target, {
            parameterSchema: {
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%from'] },
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'address', annots: ['%something'] },
                            { prim: 'address', annots: ['%to'] },
                            { prim: 'nat', annots: ['%value'] },
                        ],
                    },
                ],
            },
            expectedProcessing: false,
        });
    });

    describe('deserializeRawChangesFromNoAnnots', () => {
        it('should deserialize raw changes', () => {
            const transferDto = {
                0: 'tk1',
                1: 'tk2',
                2: new BigNumber(55),
            };

            const currentTransfers = toArray(target.deserializeRawChangesFromNoAnnots(transferDto, null!));

            expect(currentTransfers).toEqual<RawTransfer[]>([{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: transferDto[0],
                toAddress: transferDto[1],
                amount: transferDto[2],
            }]);
        });
    });

    describeMember<typeof target>('resolveContractType', () => {
        shouldResolveTypeIf('valid', target, {
            entrypointSchemas: {
                transfer: {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%from'] },
                        {
                            prim: 'pair',
                            args: [
                                { prim: 'address', annots: ['%to'] },
                                { prim: 'nat', annots: ['%value'] },
                            ],
                        },
                    ],
                },
                other: { prim: 'address', annots: ['%whatever'] },
            },
            expectedType: ContractType.FA1_2_like,
        });

        shouldResolveTypeIf('unknown entrypoint', target, {
            entrypointSchemas: {
                custom_transfer: {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%from'] },
                        {
                            prim: 'pair',
                            args: [
                                { prim: 'address', annots: ['%to'] },
                                { prim: 'nat', annots: ['%value'] },
                            ],
                        },
                    ],
                },
            },
            expectedType: null,
        });

        shouldResolveTypeIf('incompatible schema', target, {
            entrypointSchemas: {
                transfer: {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%something'] },
                        { prim: 'address', annots: ['%from'] },
                        {
                            prim: 'pair',
                            args: [
                                { prim: 'address', annots: ['%to'] },
                                { prim: 'nat', annots: ['%value'] },
                            ],
                        },
                    ],
                },
            },
            expectedType: null,
        });
    });
});
