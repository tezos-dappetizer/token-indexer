const config = require('./.eslintrc.base.js');

config.ignorePatterns.push(
    '**/dappetizer.config.ts',
);

config.rules['no-restricted-imports'] = ['error', {
    patterns: [
        "**/unit-tests/**",

        // These would create a circle.
        './public-api',
        '../public-api',
    ],
}];

config.overrides[0].rules['no-restricted-imports'] = ['error', {
    patterns: [
        // Tests should references source code files b/c bundles JS has different lifetime/decorators/etc.
        '@tezos-dappetizer/token-indexer',
    ],
}];

module.exports = config;
