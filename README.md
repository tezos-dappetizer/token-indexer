# Tezos Dappetizer Token Indexer

A package providing indexing functionality of Tezos FA1_2 and FA2 tokens.

## Initial Setup

Prerequisites:

- [Node 16](https://nodejs.org/)
- NPM 7+
   - You may need to run: `npm install -g npm`

Steps:

1. `npm install`
2. `npm run build`

## Running Locally for Development

### Token-Indexer Demo

1. Run `docker-compose up -d` in `services` folder to activate the database if needed.
2. Create and use `dappetizer.local.config.js` to override default configurations if needed.
   Set desired values for:

   - `indexing` -> `fromBlockLevel` (block level to start indexing from)
   - `tezosNode` -> `url` (url for tezos node to be used for rpc calls)
   - `database` -> `host` (address of postgres database engine, usually localhost)
   - `database` -> `port` (port of postgres database engine, usually 5432)
   - `database` -> `username` (postgres database engine username)
   - `database` -> `password` (postgres database engine password)

3. Run `npm run demo`.
