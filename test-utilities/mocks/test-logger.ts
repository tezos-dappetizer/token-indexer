import { BaseLogger, LogData, LogLevel } from '@tezos-dappetizer/utils';

export class LoggedEntry {
    constructor(
        readonly level: LogLevel,
        readonly message: string,
        readonly data: Readonly<Record<string, any>>,
    ) {}

    verify(expectedLevel: keyof typeof LogLevel, expectedData?: LogData): LoggedEntry {
        this.verifyMessagePlaceholdersInData();
        expect(this.level).toBe(LogLevel[expectedLevel]);

        if (expectedData) {
            this.verifyData(expectedData);
        }
        return this;
    }

    verifyData(expectedData: LogData): LoggedEntry {
        this.verifyMessagePlaceholdersInData();
        expect(this.data).toContainAllKeys(Object.keys(expectedData));
        expect(this.data).toMatchObject(expectedData);
        return this;
    }

    verifyMessage(...expectedSubstrs: readonly string[]): LoggedEntry {
        expectedSubstrs.forEach(s => expect(this.message).toContain(s));
        return this;
    }

    verifyMessagePlaceholdersInData(): void {
        const messageKeys = [...(this.message as any).matchAll(/\{[^\\}]+\}/gu)]
            .map(m => m.toString() as string)
            .map(s => s.substring(1, s.length - 1))
            .sort();
        const dataKeys = Object.keys(this.data).sort();

        expect(dataKeys).toEqual(expect.arrayContaining(messageKeys));
    }
}

let loggerCounter = 1;

export class TestLogger extends BaseLogger {
    readonly enabledLevels = new Set([LogLevel.Critical, LogLevel.Error, LogLevel.Warning, LogLevel.Information, LogLevel.Debug, LogLevel.Trace]);
    private readonly loggedEntries: LoggedEntry[] = [];

    constructor() {
        super(`TestCategory-${loggerCounter++}`);
    }

    logged(index: number): LoggedEntry {
        expect(this.loggedEntries.length).toBeGreaterThanOrEqual(index + 1);
        return this.loggedEntries[index]!;
    }

    loggedLast(): LoggedEntry {
        expect(this.loggedEntries).not.toBeEmpty();
        return this.loggedEntries[this.loggedEntries.length - 1]!;
    }

    loggedSingle(level?: LogLevel): LoggedEntry {
        const entries = this.loggedEntries.filter(e => level === undefined || e.level === level);
        expect(entries).toHaveLength(1);
        return entries[0]!;
    }

    verifyLoggedCount(expectedCount: number): void {
        expect(this.loggedEntries).toHaveLength(expectedCount);
    }

    verifyNothingLogged(): void {
        expect(this.loggedEntries).toEqual([]);
    }

    verifyNotLogged(unexpectedLevel: LogLevel, ...otherUnexpectedLevels: LogLevel[]) {
        for (const level of [unexpectedLevel, ...otherUnexpectedLevels]) {
            expect(this.loggedEntries.map((e) => e.level)).not.toContain(level);
        }
    }

    clear(): void {
        this.loggedEntries.length = 0;
    }

    override isEnabled(level: LogLevel): boolean {
        return this.enabledLevels.has(level);
    }

    protected override writeToLog(level: LogLevel, message: string, data: LogData): void {
        const entry = new LoggedEntry(level, message, data);
        entry.verifyMessagePlaceholdersInData();
        this.loggedEntries.push(entry);
    }
}
