import { RollupOptions } from 'rollup';
import externals from 'rollup-plugin-node-externals';
import sourceMaps from 'rollup-plugin-sourcemaps';
import typescript from 'rollup-plugin-typescript2';

export default function (cmdArgs: any): RollupOptions {
    const packageDir = getPackageDir(cmdArgs);
    const packageJson = require(`./${packageDir}/package.json`); // eslint-disable-line
    return {
        input: 'src/public-api.ts',
        output: [
            { file: packageJson.main, format: 'commonjs', sourcemap: true },
            { file: packageJson.module, format: 'es', sourcemap: true },
        ],
        watch: {
            include: 'src/**',
        },
        plugins: [
            externals({ deps: true }),
            typescript({
                useTsconfigDeclarationDir: true,
                tsconfigOverride: {
                    include: [`${packageDir}/src`],
                    compilerOptions: {
                        outDir: `${packageDir}/dist`,
                        declaration: true,
                        declarationDir: `${packageDir}/dist/types`,
                        paths: [],
                    },
                },
            }),
            sourceMaps(),
        ],
        onwarn(warning) {
            throw new Error(warning.message);// Treat warnings as errors so that missing dependencies are discovered.
        },
    };
}

function getPackageDir(cmdArgs: any): string {
    const packageName = cmdArgs.indexerPackage;

    if (typeof packageName !== 'string' || packageName.trim().length === 0) {
        throw new Error(`Missing 'package' cmd argument.`);
    }
    delete cmdArgs.indexerPackage;
    return `packages/${packageName}`;
}
