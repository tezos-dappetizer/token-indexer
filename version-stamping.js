const { resolve } = require('path');
const { writeFileSync } = require('fs-extra');
const { readdirSync } = require('fs');
const { exec } = require("child_process");

let commitHash = process.env.CI_COMMIT_SHA;

if (commitHash) {
    console.log('commit hash used from process.env.CI_COMMIT_SHA: ' + commitHash)
    processVersions();
} else {
    console.log('commit hash used from git rev-parse --short HEAD');
    exec("git rev-parse HEAD", (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            process.exit(1);
        }

        if (stderr) {
            console.log(`stderr: ${stderr}`);
        }

        commitHash = stdout.replace(/\r?\n|\r/g, "");
        console.log('commit hash: ' + commitHash);
        processVersions();
    });
}

function processVersions() {
    const packages = readdirSync('./packages');
    packages.forEach(p => writeVersion(p));
}

function writeVersion(packageName) {
    const file = resolve('./packages', packageName, 'src', 'version.ts');
    const content = `/* eslint-disable */
// IMPORTANT: THIS FILE IS AUTO GENERATED! DO NOT MANUALLY EDIT OR CHECKIN!
var packageJson = require('../package.json');
export const versionInfo = { 
    'commitHash': '${commitHash.trim()}', 
    'version': packageJson.version as string,
} as const;
/* eslint-enable */
`
    console.log(content);
    writeFileSync(file, content, { encoding: 'utf-8' });
}